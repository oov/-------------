package ;
import com.eclecticdesignstudio.motion.Actuate;
import nme.display.BitmapData;
import nme.display.DisplayObject;
import nme.display.Sprite;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.net.SharedObject;

/**
 * ...
 * @author oov
 */

class SoundModeToggleButton extends Sprite
{
	private var clickCatcher: Sprite;
	private var container: Sprite;
	private var musicOn: Bitmap;
	private var seOnly: Bitmap;
	private var mute: Bitmap;
	private var r: Float;

	public var size(default, null): Int;
	public var mode(default, null): SoundMode;

	public function new() 
	{
		super();

		mode = SoundMode.MusicOn;
		SoundManager.mode = mode;

		container = new Sprite();
		addChild(container);
	
		musicOn = Util.createEmptyBitmap();
		container.addChild(musicOn);

		seOnly = Util.createEmptyBitmap();
		container.addChild(seOnly);

		mute = Util.createEmptyBitmap();
		container.addChild(mute);

		clickCatcher = new Sprite();
		clickCatcher.addChild(Util.createEmptyBitmap());
		clickCatcher.alpha = 0;
		addChild(clickCatcher);

		container.rotation = 0;
		r = 0;

		//サウンドの設定を読み込めた場合は復元する
		var soundSetting: SharedObject = SharedObject.getLocal("sound-setting");
		var mode: Int = Std.parseInt(soundSetting.data.mode);
		for (i in 0...mode) {
			toggle(true);
		}

		addEventListener(Event.RESIZE, resize);
		clickCatcher.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
	}
	
	public function dispose(): Void {
		removeEventListener(Event.RESIZE, resize);
		clickCatcher.removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);

		musicOn.bitmapData.dispose();
		seOnly.bitmapData.dispose();
		mute.bitmapData.dispose();
	}

	private function mouseDown(e: Event): Void {
		toggle(false);
	}

	private function toggle(quick: Bool): Void {
		r -= 120;
		if (quick) {
			rotate(container, r, 0, 0);
		} else {
			Actuate.update(function(a: Float): Void {
				rotate(container, a, 0, 0);
			}, 0.5, [r + 120], [r], false);
		}

		var iMode: Int;
		switch(mode) {
			case SoundMode.MusicOn:
				mode = SoundMode.SeOnly;
				iMode = 1;
			case SoundMode.SeOnly:
				mode = SoundMode.Mute;
				iMode = 2;
			case SoundMode.Mute:
				mode = SoundMode.MusicOn;
				iMode = 0;
		}
		if (!quick){
			var soundSetting: SharedObject = SharedObject.getLocal("sound-setting");
			soundSetting.data.mode = iMode;
			try {
				soundSetting.flush();
			} catch ( e:Dynamic ) {
			}
		}
		dispatchEvent(new SoundModeEvent(mode, SoundModeEvent.SOUND_MODE_CHANGE));
		SoundManager.mode = mode;
	}

	private function resize(e: ResizeEvent): Void {
		var sz: Int = Std.int((if (e.newWidth >= e.newHeight) e.newWidth else e.newHeight) / 10);
		if (size == sz) {
			return;
		}

		size = sz;
		
		container.x = sz;
		container.y = sz;

		musicOn.bitmapData.dispose();
		musicOn.bitmapData = Util.getResizedBitmapData("sound_music_on", sz, sz);
		musicOn.x = -sz;
		musicOn.y = -sz;
		musicOn.rotation = 0;

		seOnly.bitmapData.dispose();
		seOnly.bitmapData = Util.getResizedBitmapData("sound_se_only", sz, sz);
		seOnly.x = -sz;
		seOnly.y = -sz;
		seOnly.rotation = 0;
		rotate(seOnly, 120, sz, sz);

		mute.bitmapData.dispose();
		mute.bitmapData = Util.getResizedBitmapData("sound_mute", sz, sz);
		mute.x = -sz;
		mute.y = -sz;
		mute.rotation = 0;
		rotate(mute, 240, sz, sz);
		
		clickCatcher.width = sz;
		clickCatcher.height = sz;
	}

	private static inline function degToRad(deg: Float): Float {
		return deg/180 * Math.PI;
	}

	private static function rotate(o: DisplayObject, rot: Float, x: Float, y: Float): Void {
		var rad1: Float = degToRad(o.rotation);
		var x1: Float = x * Math.cos(rad1) - y * Math.sin(rad1);
		var y1: Float = x * Math.sin(rad1) + y * Math.cos(rad1);

		var rad2: Float = degToRad(rot);
		var x2: Float = x * Math.cos(rad2) - y * Math.sin(rad2);
		var y2: Float = x * Math.sin(rad2) + y * Math.cos(rad2);

		o.rotation = rot;
		o.x += x1 - x2;
		o.y += y1 - y2;
	}
}