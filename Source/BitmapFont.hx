package ;
import aze.display.TileLayer;
import aze.display.TileSprite;
import aze.display.SparrowTilesheet;
import nme.display.BitmapData;
import nme.display.Sprite;

/**
 * ...
 * @author oov
 */
class FontCache {
	public var bmpData: BitmapData;
	public var sheet: SparrowTilesheet;
	public var widths: Array<Float>;
	public function new() {
	}
}

class BitmapFont extends Sprite
{
	private static var fontCache: Hash<FontCache> = new Hash<FontCache>();
	public static function initFont(fontName: String, fontNameAlias: String, size: Int): Void {
		var cache: FontCache = fontCache.get(fontNameAlias);
		if (cache == null) {
			cache = new FontCache();
			fontCache.set(fontNameAlias, cache);
		} else {
			if (cache.bmpData.height == size) {
				return;
			}

			cache.bmpData.dispose();
		}

		var xml: String = '<?xml version="1.0" encoding="UTF-8"?>';
		xml += '<TextureAtlas>';

		cache.widths = [];

		//全体像
		var origBitmap: BitmapData = Util.getBitmapData(fontName);
		var scaler: Float = size / origBitmap.height;
		var y: Int = origBitmap.height - 1;
		var l: Int = 0;
		var c: Int = 0;
		for (x in 0...origBitmap.width) {
			var a: Int = (origBitmap.getPixel32(x, y) >> 24) & 0xff;
			if (a == 0) {
				xml += '<SubTexture name="' + (c++) + '" x="' + Std.int(l * scaler) + '" y="0" width="' + Std.int(Math.max(1, (x - l) * scaler)) + '" height="' + Std.int(Math.max(1, y * scaler)) + '" />';
				cache.widths.push(Std.int(Math.max(1, (x - l) * scaler)));
				l = x + 1;
			} else {
				origBitmap.setPixel32(x, y, 0);
			}
		}
		xml += '</TextureAtlas>';
		cache.bmpData = Util.getResizedBitmapDataFromBitmap(origBitmap, Std.int(origBitmap.width * scaler), Std.int(origBitmap.height * scaler));
		cache.sheet = new SparrowTilesheet(cache.bmpData, xml);
		origBitmap.dispose();
	}
	
	public static function disposeFont(fontNameAlias: String): Void {
		var cache: FontCache = fontCache.get(fontNameAlias);
		if (cache != null) {
			cache.sheet = null;
			cache.bmpData.dispose();
			cache.bmpData = null;
			cache.widths = [];
			fontCache.remove(fontNameAlias);
		}
	}

	public var text(default, setText): String;
	public var align(default, setAlign): TextAlign;
	public var letterSpacing(default, setLetterSpacing): Float;
	private var cache: FontCache;
	private var tileLayer: TileLayer;
	private var sprow: SparrowTilesheet;

	public function new(fontNameAlias: String) {
		super();
		mouseChildren = false;
		mouseEnabled = false;
		cache = fontCache.get(fontNameAlias);
		tileLayer = new TileLayer(cache.sheet);
		addChild(tileLayer.view);
		text = "";
		letterSpacing = 0;
		align = TextAlign.Left;
	}
	
	private function setText(v: String): String {
		for (i in 0...tileLayer.numChildren) {
			tileLayer.removeChildAt(0);
		}

		var x: Float = 0, len: Int = v.length, c: Int;
		var sp: TileSprite;
		for (i in 0...len) {
			switch(align) {
				case TextAlign.Left:
					c = v.charCodeAt(i) - 0x20;
					sp = new TileSprite(Std.string(c));
					sp.x = x;
					x += cache.widths[c] + letterSpacing;
				case TextAlign.Right:
					c = v.charCodeAt(len - i - 1) - 0x20;
					sp = new TileSprite(Std.string(c));
					x -= cache.widths[c] + letterSpacing;
					sp.x = x;
			}
			tileLayer.addChild(sp);
		}
		
		tileLayer.render();
		return text = v;
	}
	
	private function setAlign(v: TextAlign): TextAlign {
		align = v;
		text = text;
		return v;
	}

	private function setLetterSpacing(v: Float): Float {
		letterSpacing = v;
		text = text;
		return v;
	}

	public function reposition(): Void {
		tileLayer.tilesheet = cache.sheet;
		text = text;
	}
}