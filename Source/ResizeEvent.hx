package ;

/**
 * ...
 * @author oov
 */

class ResizeEvent extends nme.events.Event
{
	public var newWidth(default, null): Int;
	public var newHeight(default, null): Int;

	public function new(type: String, newWidth_: Int, newHeight_: Int, bubbles: Bool = false, cancelable: Bool = false) 
	{
		super(type, bubbles, cancelable);
		newWidth = newWidth_;
		newHeight = newHeight_;
	}
	
}