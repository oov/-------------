package ;

/**
 * ...
 * @author oov
 */

class FieldArray<T>
{
	public var width(default, null): Int;
	public var height(default, null): Int;
	public var colors(default, null): Int;
	private var field: Array<T>;

	public function new(width_: Int, height_: Int, colors_: Int, emptyValue: T) 
	{
		width = width_;
		height = height_;
		colors = colors_;
		
		field = [];
		for (i in 0...width * height) {
			field.push(emptyValue);
		}
	}
	
	public inline function set(x: Int, y: Int, v: T): Void {
		field[x + y * height] = v;
	}

	public inline function setRaw(xy: Int, v: T): Void {
		field[xy] = v;
	}
	
	public inline function get(x: Int, y: Int): T {
		return field[x + y * height];
	}
}