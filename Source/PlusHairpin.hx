package ;

import com.eclecticdesignstudio.motion.Actuate;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.DisplayObject;
import nme.display.Sprite;
import nme.display.Stage;
import nme.display.StageAlign;
import nme.display.StageScaleMode;
import nme.events.Event;
import nme.Lib;
import nme.system.System;

/**
 * @author oov
 */
class PlusHairpin extends Sprite {
	public static var SOUND_RELOAD = "sound_reload";

	private var mainWidth: Int;
	private var mainHeight: Int;

	private var scenes: Sprite;
	private var transitions: Sprite;
	private var outer: Sprite;

	private var soundModeToggleButton: SoundModeToggleButton;
	private var nowScene: Scene;

	public function new () {
		super();

		#if flash
			Lib.current.stage.showDefaultContextMenu = false;
		#end

		Lib.current.stage.align = StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = StageScaleMode.NO_SCALE;
		initialize();
	}

	private function initialize() {
		Lib.current.stage.addEventListener(Event.RESIZE, stage_onResize);
		#if android
			//Android ではゲームに復帰した時オーディオを読み直さないと
			//復帰後一発目の発音が不発で終わる
			Lib.current.stage.addEventListener(Event.ACTIVATE, stage_onActivate);
		#end
		scenes = new Sprite();
		addChild(scenes);

		soundModeToggleButton = new SoundModeToggleButton();
		soundModeToggleButton.addEventListener(SoundModeEvent.SOUND_MODE_CHANGE, onSoundModeChange);
		addChild(soundModeToggleButton);
		SoundManager.restoreGlobalVolume();
		
		transitions = new Sprite();
		addChild(transitions);

		outer = new Sprite();
		addChild(outer);

		stage_onResize(null);

		initScene("title");
	}

	private function initScene(sceneName: String): Void {
		switch(sceneName) {
			case "title":
				nowScene = new GameTitle();
			case "menu":
				nowScene = new GameMenu();
			case "stage0":
				var a: Array<GamePhase> = [];
				var p: GamePhase;
				p = new GamePhase();
				p.seconds = 1;
				p.score = 0;
				p.fieldSeed = 3456;
				a.push(p);
				nowScene = new GameMain(sceneName, "bgm02", a);
			case "stage1":
				var a: Array<GamePhase> = [];
				var p: GamePhase;

				p = new GamePhase();
				p.seconds = 60;
				p.score = 6000;
				p.fieldSeed = 1234;
				a.push(p);
				nowScene = new GameMain(sceneName, "bgm01", a);
			case "stage2":
				var a: Array<GamePhase> = [];
				var p: GamePhase;

				p = new GamePhase();
				p.seconds = 60;
				p.score = 10000;
				p.fieldSeed = 2345;
				a.push(p);
				nowScene = new GameMain(sceneName, "bgm02", a);
			default:
				//変なことが起きたら取りあえずタイトル
				nowScene = new GameTitle();
		}
		scenes.addChild(nowScene);
		nowScene.addEventListener(CompleteSceneEvent.COMPLETE_SCENE, scene_onSceneComplete);
		reposition(nowScene);
	}

	private function scene_onSceneComplete(e: CompleteSceneEvent): Void {
		var trans: Transition = new Transition();
		function change(ev: Event): Void {
			nowScene.removeEventListener(CompleteSceneEvent.COMPLETE_SCENE, scene_onSceneComplete);
			nowScene.dispose();
			scenes.removeChild(nowScene);

			initScene(e.nextScene);
		}
		function complete(e: Event): Void {
			transitions.removeChild(trans);
			trans.removeEventListener(Transition.HALF, change);
			trans.removeEventListener(Event.COMPLETE, complete);
		}
		trans.addEventListener(Transition.HALF, change);
		trans.addEventListener(Event.COMPLETE, complete);
		reposition(trans);
		
		//Flash で謎の画面フラッシュ現象が起こるのを回避するためのワークアラウンド
		Actuate.timer(0.1).onComplete(function(): Void {
			transitions.addChild(trans);
		});
	}

	private function onSoundModeChange(e: SoundModeEvent): Void {
		SoundManager.mode = e.mode;
		scenes.dispatchEvent(new SoundModeEvent(e.mode, SoundModeEvent.SOUND_MODE_CHANGE));
	}

	private function reposition(o: DisplayObject): Void {
		o.dispatchEvent(new ResizeEvent(Event.RESIZE, mainWidth, mainHeight));
		System.gc();
	}

	private function repositionAll(): Void {
		var stageWidth: Int = Lib.current.stage.stageWidth;
		var stageHeight: Int = Lib.current.stage.stageHeight;

		//可視領域の右下
		reposition(soundModeToggleButton);
		soundModeToggleButton.x = Std.int((stageWidth - mainWidth) / 2 + mainWidth - soundModeToggleButton.size);
		soundModeToggleButton.y = Std.int((stageHeight - mainHeight) / 2 + mainHeight - soundModeToggleButton.size);

		scenes.x = Std.int((stageWidth - mainWidth) / 2);
		scenes.y = Std.int((stageHeight - mainHeight) / 2);
		for (i in 0...scenes.numChildren) {
			reposition(scenes.getChildAt(i));
		}

		transitions.x = Std.int((stageWidth - mainWidth) / 2);
		transitions.y = Std.int((stageHeight - mainHeight) / 2);
		for (i in 0...transitions.numChildren) {
			reposition(transitions.getChildAt(i));
		}

	}

	private function stage_onActivate(e: Event): Void {
		//オーディオを復活させないといけないのでイベントで通知
		for (i in 0...scenes.numChildren) {
			scenes.getChildAt(i).dispatchEvent(new Event(PlusHairpin.SOUND_RELOAD));
		}
		System.gc();
	}

	private function stage_onResize(e: Event): Void {
		var stageWidth: Int = Lib.current.stage.stageWidth;
		var stageHeight: Int = Lib.current.stage.stageHeight;

		//画面の大きさは iPhone の 2:3 及び 3:2 を基準に決める
		//これに対してはみ出す場合はある程度余白を設けて調節する
		var ratiow: Int = 2, ratioh: Int = 3;

		if (stageWidth >= stageHeight) {
			var t: Int = ratiow;
			ratiow = ratioh;
			ratioh = t;
		}

		//縦と横の両方を計算してはみ出さない方を採用する
		var w1: Int, h1: Int;
		var w2: Int, h2: Int;
		w1 = stageWidth;
		h1 = Std.int(stageWidth / ratiow * ratioh);
		w2 = Std.int(stageHeight / ratioh * ratiow);
		h2 = stageHeight;
		if (w1 <= stageWidth && h1 <= stageHeight) {
			mainWidth = w1;
			mainHeight = h1;
		} else {
			mainWidth = w2;
			mainHeight = h2;
		}

		repositionAll();

		for (i in 0...outer.numChildren) {
			cast(outer.getChildAt(0), Bitmap).bitmapData.dispose();
			outer.removeChildAt(0);
		}
		var bmp: Bitmap;
		if (scenes.y != 0) {
			bmp = Util.createBitmap(new BitmapData(stageWidth, Std.int(scenes.y)+1, false, 0));
			outer.addChild(bmp);
			bmp = Util.createBitmap(bmp.bitmapData);
			bmp.y = scenes.y + mainHeight;
			outer.addChild(bmp);
		} else if (scenes.x != 0) {
			bmp = Util.createBitmap(new BitmapData(Std.int(scenes.x)+1, stageHeight, false, 0));
			outer.addChild(bmp);
			bmp = Util.createBitmap(bmp.bitmapData);
			bmp.x = scenes.x + mainWidth;
			outer.addChild(bmp);
		}
	}

	public static function main () {		
		Lib.current.addChild(new PlusHairpin ());
	}
}