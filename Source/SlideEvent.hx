package ;
import nme.events.Event;

/**
 * ...
 * @author oov
 */

class SlideEvent extends Event
{
	public var slideTo: SlideTo;
	public function new(slideTo_: SlideTo) {
		super(Field.SLIDE);
		slideTo = slideTo_;
	}
}