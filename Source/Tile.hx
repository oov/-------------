package ;
import aze.display.SparrowTilesheet;
import aze.display.TileSprite;
import nme.display.BitmapData;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
 * ...
 * @author oov
 */

class Tile extends TileSprite
{
	public static inline var TILES = 6;
	private static var nowTileSize: Int = 0;
	private static var bmpData: BitmapData = null;
	public static var sheet: SparrowTilesheet = null;
	public static function initTileBitmapData(tileSize: Int): Void {
		if (bmpData != null && bmpData.height == tileSize) {
			return;
		}

		if (bmpData != null){
			bmpData.dispose();
		}
		
		bmpData = new BitmapData(tileSize * TILES, tileSize * 5, true, 0);

		var xml: String = '<?xml version="1.0" encoding="UTF-8"?>';
		xml += '<TextureAtlas>';

		var rect: Rectangle = new Rectangle(0, 0, tileSize, tileSize);
		var pt: Point = new Point();
		for (i in 1...TILES + 1) {
			var tmp: BitmapData = Util.getResizedBitmapData("tile" + i, tileSize, tileSize);
			bmpData.copyPixels(tmp, rect, pt);
			tmp.dispose();
			xml += '<SubTexture name="' + (i-1) + '" x="' + Std.int(pt.x) + '" y="0" width="' + tileSize + '" height="' + tileSize + '" />';
			pt.x += tileSize;
		}

		//目隠し用テクスチャ
		var tmp: BitmapData = Util.getResizedBitmapData("blind", tileSize * 3, tileSize * 3);
		bmpData.copyPixels(tmp, new Rectangle(0, 0, tileSize * 3, tileSize * 3), new Point(0, tileSize * 2));
		tmp.dispose();
		xml += '<SubTexture name="blind" x="0" y="' + (tileSize * 2) + '" width="' + (tileSize * 3) + '" height="' + (tileSize * 3) + '" />';

		xml += '</TextureAtlas>';
		sheet = new SparrowTilesheet(bmpData, xml);
		nowTileSize = tileSize;
	}
	
	public static function isSameColor(tileA: Tile, tileB: Tile): Bool {
		if (Type.getClass(tileA) == SpecialTile || Type.getClass(tileB) == SpecialTile) {
			return true;
		}
		return tileA.tileColor == tileB.tileColor;
	}

	public var tileColor(default, null): Int;
	public var fieldX(default, setFieldX): Float;
	public var fieldY(default, setFieldY): Float;

	public function new(tileColor_: Int) {
		super(Std.string(tileColor_));
		tileColor = tileColor_;
	}

	private function setFieldX(v: Float): Float {
		fieldX = v;
		x = (v + 0.5) * nowTileSize;
		return v;
	}

	private function setFieldY(v: Float): Float {
		fieldY = v;
		y = (v + 0.5) * nowTileSize;
		return v;
	}
}