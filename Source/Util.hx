package ;
import haxe.BaseCode;
import haxe.io.Bytes;
import haxe.Md5;
import nme.Assets;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.PixelSnapping;
import nme.geom.Matrix;
import nme.geom.Point;
import nme.geom.Rectangle;
import nme.Lib;
import nme.net.URLRequest;

/**
 * ...
 * @author oov
 */

class Util 
{
	#if cpp
	/**
	 * id で指定されたリソースを key でスクランブル処理して filename に保存する
	 * @param	key
	 * @param	filename
	 * @param	id
	 */
	public static function saveScrambledImage(key: Int, filename: String, id: String): Void {
		var bmpd: BitmapData = Assets.getBitmapData(id, false);
		scrambleBitmapData(key, bmpd);
		savePNG(filename, bmpd);
	}

	/**
	 * 画像を png で保存する（cppのみ）
	 * @param	filename
	 * @param	bmpd
	 */
	public static function savePNG(filename: String, bmpd: BitmapData): Void {
		var f = cpp.io.File.write(filename, true);
		f.writeString(bmpd.encode('png').asString());
		f.close();
	}
	#end

	public static inline function shuffleArray<T>(rand: XorshiftRNG, a: Array<T>): Void {
		var i: Int = a.length;
		while (i != 0) {
			var j: Int = Std.int(rand.next() * i);
			var t: T = a[--i];
			a[i] = a[j];
			a[j] = t;
		}
	}

	/**
	 * 画像をスクランブル処理する
	 * 2度掛けると元に戻る
	 * 幅と高さが16で割り切れないサイズの画像に対して行うとぶっ壊れると思うので注意すること
	 * @param	key
	 * @param	bmpData
	 */
	public static function scrambleBitmapData(key: Int, bmpData: BitmapData): Void {

		var points: Array<Point> = [];
		var x: Int = 0, y: Int = 0, step: Int = 16;
		while (y < bmpData.height) {
			x = 0;
			while (x < bmpData.width) {
				points.push(new Point(x, y));
				x += step;
			}
			y += step;
		}
		shuffleArray(new XorshiftRNG(key), points);

		var tmpData: BitmapData = new BitmapData(bmpData.width, bmpData.height, false);

		var i: Int = 0;
		var r: Rectangle = new Rectangle(0, 0, step, step);
		while (i < points.length) {
			var p1 = points[i++];
			var p2 = points[i++];
			r.x = p1.x;
			r.y = p1.y;
			tmpData.copyPixels(bmpData, r, p2);
			r.x = p2.x;
			r.y = p2.y;
			tmpData.copyPixels(bmpData, r, p1);
		}

		bmpData.draw(tmpData);

		tmpData.dispose();
	}

	public static inline function getBitmapData(id: String): BitmapData {
		return Assets.getBitmapData(id, false);
	}

	public static function getResizedBitmapDataFromBitmap(bmps: BitmapData, width: Float, height: Float): BitmapData {
		var wi = Std.int(Math.max(1, width)), hi = Std.int(Math.max(1, height));
		var bmpd: BitmapData = new BitmapData(wi, hi, true, 0);
		var tmp: BitmapData = null;
		var w: Int = bmps.width, h: Int = bmps.height;
		while (wi << 1 < w && hi << 1 < h) {
			w = w >> 1;
			h = h >> 1;

			var oldTmp: BitmapData = tmp;
			tmp = new BitmapData(w, h, true, 0);
			var mat: Matrix = new Matrix();
			mat.scale(0.5, 0.5);
			tmp.draw(bmps, mat, null, null, null, true);
			bmps = tmp;
			if (oldTmp != null) oldTmp.dispose();
		}
		var mat: Matrix = new Matrix();
		mat.scale(bmpd.width / w, bmpd.height / h);
		bmpd.draw(bmps, mat, null, null, null, true);
		return bmpd;
	}

	public static function getResizedBitmapData(id: String, width: Float, height: Float): BitmapData {
		var bmps: BitmapData = getBitmapData(id);
		var bmpd: BitmapData = getResizedBitmapDataFromBitmap(bmps, width, height);
		bmps.dispose();
		return bmpd;
	}

	private static var dummy: BitmapData = new BitmapData(1, 1);
	public static inline function createBitmap(bmpd: BitmapData): Bitmap {
		var bmp: Bitmap;
		//html5 の時は左上にチラつく要素が出るのでワークアラウンドが必要
		#if js
			bmp = new Bitmap(dummy, PixelSnapping.AUTO);
			bmp.bitmapData = bmpd;
		#else
			bmp = new Bitmap(bmpd, PixelSnapping.AUTO);
		#end
		return bmp;
	}

	public static inline function createEmptyBitmap(): Bitmap {
		return createBitmap(new BitmapData(1, 1));
	}
	
	public static function hexDec(s: String): Int {
		switch(s) {
			case "0": return 0;
			case "1": return 1;
			case "2": return 2;
			case "3": return 3;
			case "4": return 4;
			case "5": return 5;
			case "6": return 6;
			case "7": return 7;
			case "8": return 8;
			case "9": return 9;
			case "a": return 10;
			case "b": return 11;
			case "c": return 12;
			case "d": return 13;
			case "e": return 14;
			case "f": return 15;
			default: return 0;
		}
	}
	public static function buildPresentURL(stageId: String, score: Int): String {
		var BASE64CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

		var r = new XorshiftRNG();
		var key = Std.int(r.next() * 0xffffff);
		r.setSeed(key);
		var time = Std.int(Date.now().getTime() / 1000);
		var sum = 0;
		var c: Int;
		
		var bin = new haxe.io.BytesBuffer();
		c = (key >> 16) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c);
		c = (key >>  8) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c);
		c = (key >>  0) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c);
		
		var stg5 = Md5.encode(stageId);
		
		c = (hexDec(stg5.charAt(0)) << 4)|hexDec(stg5.charAt(1));
		sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		
		c = (hexDec(stg5.charAt(2)) << 4)|hexDec(stg5.charAt(3));
		sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		
		c = (hexDec(stg5.charAt(4)) << 4)|hexDec(stg5.charAt(5));
		sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		
		c = (hexDec(stg5.charAt(6)) << 4)|hexDec(stg5.charAt(7));
		sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		
		c = (score >> 24) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		c = (score >> 16) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		c = (score >>  8) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		c = (score >>  0) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));

		c = (time >> 24) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		c = (time >> 16) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		c = (time >>  8) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));
		c = (time >>  0) & 0xff; sum = (sum ^ c) & 0xff;
		bin.addByte(c ^ (Std.int(r.nextUInt32()) & 0xff));

		bin.addByte(sum);
		var bce = new BaseCode(haxe.io.Bytes.ofString(BASE64CHARS));
		var bc = bce.encodeBytes(bin.getBytes());
		return "http://googleplus-1st-aniv-iwatte-mirror.oov.ch/plus-hairpin/present/#s=" + StringTools.urlEncode(stageId) + "&sc=" + StringTools.urlEncode(Std.string(score)) + "&k=" + StringTools.urlEncode(bc.toString());
	}

	public static inline function getURL(url: String): Void {
		//HTML5 で Lib.getURL を使うとポップアップになってしまうので分岐する
		#if js
			js.Lib.window.location.href = url;
		#else
			Lib.getURL(new URLRequest(url));
		#end
	}
}