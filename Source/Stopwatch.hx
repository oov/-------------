package ;
import haxe.Timer;

/**
 * ...
 * @author oov
 */

class Stopwatch 
{
	private var startAt: Float;
	private var pausedAt: Float;
	private var pauseCount: Int;
	public var paused(getPaused, null): Bool;
	public var step(getStep, setStep): Float;

	public function new() {
		pauseCount = 0;
		startAt = Timer.stamp();
	}
	
	public function pause(): Void {
		if (pauseCount == 0) {
			pausedAt = getStep();
		}
		++pauseCount;
	}
	
	public function getPaused(): Bool {
		return pauseCount != 0;
	}

	public function unpause(): Void {
		if (--pauseCount != 0) {
			return;
		}
		startAt = Timer.stamp() - pausedAt;
	}
	
	private function getStep(): Float {
		if (pauseCount != 0) {
			return pausedAt;
		}
		return Timer.stamp() - startAt;
	}
	
	private function setStep(v: Float): Float {
		if (pauseCount != 0) {
			pausedAt = v;
		} else {
			startAt = Timer.stamp() - v;
		}
		return v;
	}
}