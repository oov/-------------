package ;
import nme.events.Event;

/**
 * ...
 * @author oov
 */

class SoundModeEvent extends Event
{
	public static var SOUND_MODE_CHANGE = "sound_mode_change";
	public var mode: SoundMode;
	public function new(mode_: SoundMode, type: String, bubbles: Bool = false, cancelable: Bool = false)
	{
		super(type, bubbles, cancelable);
		mode = mode_;
	}
	
}