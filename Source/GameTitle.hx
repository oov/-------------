package ;
import com.eclecticdesignstudio.motion.Actuate;
import haxe.Timer;
import nme.Assets;
import nme.display.Sprite;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.geom.Point;
import nme.media.Sound;
import nme.media.SoundChannel;

/**
 * ...
 * @author oov
 */

class GameTitle extends Scene 
{
	private var title: Bitmap;
	private var logo: Bitmap;
	private var logoPos: Point;
	private var versionText: BitmapFont;
	private var selectSE: Sound;

	public function new() {
		super();
		mouseChildren = false;

		title = Util.createEmptyBitmap();
		addChild(title);

		logo = Util.createEmptyBitmap();
		logo.alpha = 0;
		logoPos = new Point(0, 0);
		addChild(logo);

		BitmapFont.initFont("font_anzu_white", "version", 1);
		versionText = new BitmapFont("version");
		versionText.text = "20120921";
		versionText.align = TextAlign.Right;
		addChild(versionText);

		initSound();

		addEventListener(Event.RESIZE, resize);
		addEventListener(Event.ENTER_FRAME, enterFrame);
		addEventListener(PlusHairpin.SOUND_RELOAD, soundReload);
		addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);

		Actuate.timer(0.5).onComplete(function(): Void {
			Actuate.tween(logo, 1, { alpha: 1 });
		});
	}

	private function enterFrame(e: Event): Void {
		logo.x = logoPos.x;
		logo.y = logoPos.y + Math.sin(Timer.stamp()*2.5) * logo.bitmapData.height * 0.025;
	}

	private function mouseDown(e: MouseEvent): Void {
		removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
		SoundManager.playSE(selectSE);
		Actuate.timer(0.3).onComplete(function():Void {		
			dispatchEvent(new CompleteSceneEvent("menu"));
		});
	}

	private function soundReload(e: Event): Void {
		initSound();
	}

	private function initSound(): Void {
		selectSE = Assets.getSound("title_select");
	}

	override function dispose(): Void {
		selectSE = null;
		removeEventListener(Event.RESIZE, resize);
		removeEventListener(Event.ENTER_FRAME, enterFrame);
		removeEventListener(PlusHairpin.SOUND_RELOAD, soundReload);

		removeChild(logo);
		logo.bitmapData.dispose();

		removeChild(title);
		title.bitmapData.dispose();

		BitmapFont.disposeFont("version");
	}

	private function resize(e: ResizeEvent): Void {
		var sz = if (e.newWidth >= e.newHeight) e.newWidth else e.newHeight;
		var s = if (e.newWidth <= e.newHeight) e.newWidth else e.newHeight;
		if (title.bitmapData.width != sz){
			title.bitmapData.dispose();
			title.bitmapData = Util.getResizedBitmapData("title", sz, sz);

			logo.bitmapData.dispose();
			logo.bitmapData = Util.getResizedBitmapData("title_logo", (sz * 3) * 0.14, (sz * 2) * 0.14);
		}

		BitmapFont.initFont("font_anzu_white", "version", Std.int(sz * 0.03));
		versionText.letterSpacing = - sz * 0.003;
		versionText.reposition();

		if (e.newWidth >= e.newHeight) {
			title.x = 0;
			title.y = -sz / 3 / 2;
		} else {
			title.x = -sz / 3 / 2;
			title.y = 0;
		}

		var oX = 0.0, oY = 0.0;
		if (e.newWidth >= e.newHeight) {
			oX = s * 0.25;
		} else {
			oY = s * 0.25;
		}
		logoPos.x = oX + sz * -0.04;
		logoPos.y = oY + sz * 0;
		versionText.x = oX + s - sz * 0.02;
		versionText.y = oY + s - sz * 0.03;
		dispatchEvent(new Event(Event.ENTER_FRAME));
	}
}