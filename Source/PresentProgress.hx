package ;
import aze.display.SparrowTilesheet;
import aze.display.TileLayer;
import aze.display.TileSprite;
import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.actuators.GenericActuator;
import com.eclecticdesignstudio.motion.easing.Cubic;
import com.eclecticdesignstudio.motion.easing.Linear;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Sprite;
import nme.events.Event;
import nme.geom.Point;
import nme.geom.Rectangle;

/**
 * ...
 * @author oov
 */

class PresentProgress extends Sprite
{
	private static var DIVIDER = 16;

	public var progress(getProgress, setProgress): Float;

	private var moved: Bool;

	private var rand: XorshiftRNG;
	private var id: String;
	private var key: Int;
	private var masterBitmapData: BitmapData;
	private var canvas: Bitmap;
	private var tilePositions: Array<Point>;
	private var intProgress: Int;
	private var moving: Int;
	private var particleDistance: Float;
	private var blockSize: Float;
	
	private var sprow: SparrowTilesheet;
	private var tileLayer: TileLayer;

	private var animating: Array<GenericActuator>;

	public function new(id_: String, key_: Int) {
		super();

		id = id_;
		key = key_;
		moved = false;

		animating = [];

		rand = new XorshiftRNG();

		moving = 0;
		tilePositions = [];
		for (y in 0...DIVIDER) {
			for (x in 0...DIVIDER) {
				tilePositions.push(new Point(x, y));
			}
		}
		Util.shuffleArray(new XorshiftRNG(), tilePositions);
		
		masterBitmapData = new BitmapData(1, 1);
		canvas = Util.createEmptyBitmap();
		addChild(canvas);

		tileLayer = new TileLayer(null);
		addChild(tileLayer.view);
		intProgress = 0;
		progress = 0;
		
		addEventListener(Event.ENTER_FRAME, enterFrame);
	}

	public function enterFrame(e: Event): Void {
		if (moved){
			tileLayer.render();
			moved = false;
		}
	}

	public function dispose(): Void {
		canvas.bitmapData.dispose();
		masterBitmapData.dispose();
		masterBitmapData = null;
		removeChild(canvas);
	}

	public function reset(): Void {
		for (i in 0...animating.length) {
			animating[0].stop(null, true, true);
		}
		intProgress = 0;
		buildImage();
	}

	private function getProgress(): Float {
		return (intProgress+moving) / tilePositions.length;
	}

	private function setProgress(v: Float): Float {
		//値の増減をチェック
		var vi: Int = Std.int(v * tilePositions.length);
		var d: Int = vi - intProgress - moving;
		if (d < 0) {
		} else if (d > 0) {
			var c: Float = 0;
			for (i in intProgress+moving...Std.int(Math.min(intProgress+moving+d, tilePositions.length))) {
				shotCube(tilePositions[i]).delay(c += 0.1);
			}
		}
		return vi+d;
	}

	private function shotCube(pt: Point): GenericActuator {
		var r: Float = rand.next() * Math.PI * 2 - Math.PI;
		var fx: Float = Math.sin(r);
		var fy: Float = Math.cos(r);
		
		var sp: TileSprite = new TileSprite(Std.int(pt.x) + ',' + Std.int(pt.y));
		tileLayer.addChild(sp);
		sp.alpha = 0;
		++moving;
		var ret = cast(Actuate.update(function(ax: Float, ay: Float, a: Float): Void {
			sp.x = ax * blockSize;
			sp.y = ay * blockSize;
			sp.alpha = a;
			moved = true;
		}, 0.5, [pt.x + fx * 10, pt.y + fy * 10, 0], [pt.x + 0.5, pt.y + 0.5, 1], false).ease(Cubic.easeOut), GenericActuator);
		ret.onComplete(function(): Void {
			Actuate.update(function(s: Float, a: Float): Void {
				sp.scale = s;
				sp.alpha = a;
				sp.rotation = (1-a) * 3;
				moved = true;
			}, 0.3, [1, 1], [3, 0], false).ease(Linear.easeNone).onComplete(function(): Void {
				tileLayer.removeChild(sp);
				moved = true;
			});
			draw(pt);
			--moving;
			++intProgress;

			for (i in 0...animating.length) {
				if (ret == animating[i]) {
					animating.splice(i, 1);
					break;
				}
			}
		});
		animating.push(ret);
		return ret;
	}

	private function buildXML(sz: Int): String {
		var r: String = '<?xml version="1.0" encoding="UTF-8"?>';
		r += '<TextureAtlas>';
		for (y in 0...DIVIDER) {
			for (x in 0...DIVIDER) {
				r += '<SubTexture name="' + x + ',' + y + '" x="' + Std.int(blockSize * x) + '" y="' + Std.int(blockSize * y) + '" width="' + Std.int(blockSize + 0.5) + '" height="' + Std.int(blockSize + 0.5) + '" />';
			}
		}
		r += '</TextureAtlas>';
		return r;
	}

	public function resize(sz: Int): Void {
		if (masterBitmapData.width == sz) {
			return;
		}

		particleDistance = sz * 3;
		blockSize = sz / DIVIDER;

		masterBitmapData.dispose();
		//画像を読み込んでデコード復号化してリサイズ
		masterBitmapData = Util.getBitmapData(id);
		if (key != 0){
			Util.scrambleBitmapData(key, masterBitmapData);
		}
		masterBitmapData = Util.getResizedBitmapDataFromBitmap(masterBitmapData, sz, sz);

		canvas.bitmapData.dispose();
		var bmpData: BitmapData = new BitmapData(sz, sz, false, 0);
		canvas.bitmapData = bmpData;

		buildImage();

		sprow = new SparrowTilesheet(masterBitmapData, buildXML(sz));
		tileLayer.tilesheet = sprow;

		moved = true;
	}
	
	private function buildImage(): Void {
		var sz: Int = masterBitmapData.width;
		var bmpData: BitmapData = canvas.bitmapData;
		//モノトーンのベースを作る
		masterBitmapData.lock();
		bmpData.lock();
		for (y in 0...sz) {
			for (x in 0...sz) {
				var c: Int = masterBitmapData.getPixel(x, y);
				bmpData.setPixel(x, y, mono(c));
			}
		}

		//現在の達成率に合わせて合成
		for (i in 0...intProgress) {
			draw(tilePositions[i]);
		}

		bmpData.unlock();
		masterBitmapData.unlock();
	}

	private function draw(pt: Point): Void {
		var p = new Point(Math.floor(pt.x * blockSize), Math.floor(pt.y * blockSize));
		if (masterBitmapData != null) {
			canvas.bitmapData.copyPixels(masterBitmapData, new Rectangle(p.x, p.y, blockSize + 1, blockSize + 1), p);
		}
	}

	/**
	 * 暗めのモノトーンカラーへ変換する
	 * @param	col
	 * @return
	 */
	private static inline function mono(col: Int): Int {
		var v: Int = Std.int(((col & 0xff) + ((col >> 8) & 0xff) + ((col >> 16) & 0xff))) >> 3;
		return v | (v << 8) | (v << 16);
	}
}