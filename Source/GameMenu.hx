package ;
import com.eclecticdesignstudio.motion.Actuate;
import nme.Assets;
import nme.display.Sprite;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.media.Sound;
import nme.media.SoundChannel;
import nme.net.SharedObject;

/**
 * ...
 * @author oov
 */

class GameMenu extends Scene 
{
	private var menu: Bitmap;

	private var effectPlane: Sprite;

	private var backButton: Sprite;

	private var stage1Button: Sprite;
	private var stage1PresentButton: Sprite;
	private var stage1HighScore: BitmapFont;

	private var stage2Button: Sprite;
	private var stage2PresentButton: Sprite;
	private var stage2HighScore: BitmapFont;

	private var bgmVol25Button: Sprite;
	private var bgmVol50Button: Sprite;
	private var bgmVol75Button: Sprite;
	private var bgmVol100Button: Sprite;

	private var bgm: Sound;
	private var bgmChannel: SoundChannel;

	private var selectSE: Sound;
	private var volumeSE: Sound;

	private var navigated: Bool;

	public function new() {
		super();

		navigated = false;

		BitmapFont.initFont("font_anzu_white", "highscore", 1);

		menu = Util.createEmptyBitmap();
		addChild(menu);

		addChild(effectPlane = new Sprite());
	
		addChild(backButton = new Sprite());
		backButton.addChild(Util.createEmptyBitmap());
		backButton.addEventListener(MouseEvent.MOUSE_DOWN, backButton_onMouseDown);

		var soundSetting: SharedObject = SharedObject.getLocal("stageClear");

		var score = Std.int(soundSetting.data.stage1);

		addChild(stage1Button = new Sprite());
		stage1Button.addChild(Util.createEmptyBitmap());
		stage1Button.addEventListener(MouseEvent.MOUSE_DOWN, stage1Button_onMouseDown);

		addChild(stage1HighScore = new BitmapFont("highscore"));
		stage1HighScore.align = TextAlign.Right;
		stage1HighScore.text = Std.string(score);

		addChild(stage1PresentButton = new Sprite());
		stage1PresentButton.addChild(Util.createEmptyBitmap());
		stage1PresentButton.addEventListener(MouseEvent.MOUSE_DOWN, stage1PresentButton_onMouseDown);
		stage1PresentButton.visible = score > 0;

		score = Std.int(soundSetting.data.stage2);

		addChild(stage2Button = new Sprite());
		stage2Button.addChild(Util.createEmptyBitmap());
		stage2Button.addEventListener(MouseEvent.MOUSE_DOWN, stage2Button_onMouseDown);

		addChild(stage2HighScore = new BitmapFont("highscore"));
		stage2HighScore.align = TextAlign.Right;
		stage2HighScore.text = Std.string(score);

		addChild(stage2PresentButton = new Sprite());
		stage2PresentButton.addChild(Util.createEmptyBitmap());
		stage2PresentButton.addEventListener(MouseEvent.MOUSE_DOWN, stage2PresentButton_onMouseDown);
		stage2PresentButton.visible = score > 0;

		addChild(bgmVol25Button = new Sprite());
		bgmVol25Button.addChild(Util.createEmptyBitmap());
		bgmVol25Button.addEventListener(MouseEvent.MOUSE_DOWN, bgmVol25Button_onMouseDown);

		addChild(bgmVol50Button = new Sprite());
		bgmVol50Button.addChild(Util.createEmptyBitmap());
		bgmVol50Button.addEventListener(MouseEvent.MOUSE_DOWN, bgmVol50Button_onMouseDown);

		addChild(bgmVol75Button = new Sprite());
		bgmVol75Button.addChild(Util.createEmptyBitmap());
		bgmVol75Button.addEventListener(MouseEvent.MOUSE_DOWN, bgmVol75Button_onMouseDown);

		addChild(bgmVol100Button = new Sprite());
		bgmVol100Button.addChild(Util.createEmptyBitmap());
		bgmVol100Button.addEventListener(MouseEvent.MOUSE_DOWN, bgmVol100Button_onMouseDown);

		updateVolumeState();

		initSound();

		addEventListener(Event.RESIZE, resize);
		addEventListener(PlusHairpin.SOUND_RELOAD, soundReload);

		Actuate.timer(0.25).onComplete(function(): Void {
			bgmChannel = SoundManager.playMusic(bgm, true);
		});
	}

	override function dispose(): Void {
		if (bgmChannel != null) {
			bgmChannel.stop();
			bgmChannel = null;
		}
		bgm = null;

		selectSE = null;
		removeEventListener(Event.RESIZE, resize);
		removeEventListener(PlusHairpin.SOUND_RELOAD, soundReload);

		removeChild(backButton);
		cast(backButton.removeChildAt(0), Bitmap).bitmapData.dispose();
		backButton.removeEventListener(MouseEvent.MOUSE_DOWN, backButton_onMouseDown);

		removeChild(stage1Button);
		cast(stage1Button.removeChildAt(0), Bitmap).bitmapData.dispose();
		stage1Button.removeEventListener(MouseEvent.MOUSE_DOWN, stage1Button_onMouseDown);

		removeChild(stage1PresentButton);
		cast(stage1PresentButton.removeChildAt(0), Bitmap).bitmapData.dispose();
		stage1PresentButton.removeEventListener(MouseEvent.MOUSE_DOWN, stage1Button_onMouseDown);

		removeChild(stage1HighScore);

		removeChild(stage2Button);
		cast(stage2Button.removeChildAt(0), Bitmap).bitmapData.dispose();
		stage2Button.removeEventListener(MouseEvent.MOUSE_DOWN, stage2Button_onMouseDown);

		removeChild(stage2PresentButton);
		cast(stage2PresentButton.removeChildAt(0), Bitmap).bitmapData.dispose();
		stage2PresentButton.removeEventListener(MouseEvent.MOUSE_DOWN, stage2Button_onMouseDown);

		removeChild(stage2HighScore);

		removeChild(bgmVol25Button);
		cast(bgmVol25Button.removeChildAt(0), Bitmap).bitmapData.dispose();
		bgmVol25Button.removeEventListener(MouseEvent.MOUSE_DOWN, bgmVol25Button_onMouseDown);

		removeChild(bgmVol50Button);
		cast(bgmVol50Button.removeChildAt(0), Bitmap).bitmapData.dispose();
		bgmVol50Button.removeEventListener(MouseEvent.MOUSE_DOWN, bgmVol50Button_onMouseDown);

		removeChild(bgmVol75Button);
		cast(bgmVol75Button.removeChildAt(0), Bitmap).bitmapData.dispose();
		bgmVol75Button.removeEventListener(MouseEvent.MOUSE_DOWN, bgmVol75Button_onMouseDown);

		removeChild(bgmVol100Button);
		cast(bgmVol100Button.removeChildAt(0), Bitmap).bitmapData.dispose();
		bgmVol100Button.removeEventListener(MouseEvent.MOUSE_DOWN, bgmVol100Button_onMouseDown);

		removeChild(effectPlane);
		
		removeChild(menu);
		menu.bitmapData.dispose();
		
		BitmapFont.disposeFont("highscore");
	}

	private function backButton_onMouseDown(e: MouseEvent): Void {
		if (navigated) return;
		navigated = true;
		buttonEffect(backButton);
		SoundManager.playSE(selectSE);
		dispatchEvent(new CompleteSceneEvent("title"));
	}

	private function stage1Button_onMouseDown(e: MouseEvent): Void {
		if (navigated) return;
		navigated = true;
		buttonEffect(stage1Button);
		SoundManager.playSE(selectSE);
		Actuate.timer(0.5).onComplete(function():Void {
			dispatchEvent(new CompleteSceneEvent("stage1"));
		});
	}

	private function stage1PresentButton_onMouseDown(e: MouseEvent): Void {
		buttonEffect(stage1PresentButton);
		Util.getURL(Util.buildPresentURL("stage1", Std.parseInt(stage1HighScore.text)));
	}

	private function stage2Button_onMouseDown(e: MouseEvent): Void {
		if (navigated) return;
		navigated = true;
		buttonEffect(stage2Button);
		SoundManager.playSE(selectSE);
		Actuate.timer(0.5).onComplete(function():Void {
			dispatchEvent(new CompleteSceneEvent("stage2"));
		});
	}

	private function stage2PresentButton_onMouseDown(e: MouseEvent): Void {
		buttonEffect(stage2PresentButton);
		Util.getURL(Util.buildPresentURL("stage2", Std.parseInt(stage2HighScore.text)));
	}

	private function updateVolumeState(): Void {
		var v = SoundManager.globalVolume;
		bgmVol25Button.alpha = 0.25;
		bgmVol50Button.alpha = 0.25;
		bgmVol75Button.alpha = 0.25;
		bgmVol100Button.alpha = 0.25;

		if (0.24 < v && v < 0.26) {
			buttonEffect(bgmVol25Button);
		} else if (0.49 < v && v < 0.51) {
			buttonEffect(bgmVol50Button);
		} else if (0.74 < v && v < 0.76) {
			buttonEffect(bgmVol75Button);
		} else if (0.99 < v && v < 1.01) {
			buttonEffect(bgmVol100Button);
		}

		if (0.24 < v) {
			bgmVol25Button.alpha = 1;
		}
		if (0.49 < v) {
			bgmVol50Button.alpha = 1;
		}
		if (0.74 < v) {
			bgmVol75Button.alpha = 1;
		}
		if (0.99 < v) {
			bgmVol100Button.alpha = 1;
		}
	}

	private function buttonEffect(b: Sprite): Void {
		var bb = cast(b.getChildAt(0), Bitmap).bitmapData;

		//エフェクト用の Sprite を作成
		var sp = new Sprite();
		var bmp = Util.createBitmap(bb);
		sp.x = b.x;
		sp.y = b.y;
		sp.addChild(bmp);
		effectPlane.addChild(sp);
		Actuate.tween(sp, 1, { x: sp.x - bb.width * 0.5, y: sp.y - bb.height * 0.5, scaleX: 2.0, scaleY: 2.0, alpha: 0 }, true).onComplete(function():Void {
			effectPlane.removeChild(sp);
			sp.removeChildAt(0);
		});
	}

	private function bgmVol25Button_onMouseDown(e: MouseEvent): Void {
		SoundManager.globalVolume = 0.25;
		SoundManager.playSE(volumeSE);
		updateVolumeState();
	}

	private function bgmVol50Button_onMouseDown(e: MouseEvent): Void {
		SoundManager.globalVolume = 0.50;
		SoundManager.playSE(volumeSE);
		updateVolumeState();
	}

	private function bgmVol75Button_onMouseDown(e: MouseEvent): Void {
		SoundManager.globalVolume = 0.75;
		SoundManager.playSE(volumeSE);
		updateVolumeState();
	}

	private function bgmVol100Button_onMouseDown(e: MouseEvent): Void {
		SoundManager.globalVolume = 1.00;
		SoundManager.playSE(volumeSE);
		updateVolumeState();
	}

	private function soundReload(e: Event): Void {
		initSound();
	}

	private function initSound(): Void {
		if (bgm == null){
			bgm = Assets.getSound("bgm04");
		}

		selectSE = Assets.getSound("menu_select");
		volumeSE = Assets.getSound("menu_volume");
	}

	private function resize(e: ResizeEvent): Void {
		var sz = if (e.newWidth >= e.newHeight) e.newWidth else e.newHeight;
		var s = if (e.newWidth <= e.newHeight) e.newWidth else e.newHeight;
		if (menu.bitmapData.width != sz){
			menu.bitmapData.dispose();
			menu.bitmapData = Util.getResizedBitmapData("menu", sz, sz);
		}
		if (e.newWidth >= e.newHeight) {
			menu.x = 0;
			menu.y = -sz / 3 / 2;
		} else {
			menu.x = -sz / 3 / 2;
			menu.y = 0;
		}

		var sqButtonSize = s * 0.15;
		var bmp: Bitmap;

		bmp = cast(backButton.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_back", sqButtonSize, sqButtonSize);

		
		bmp = cast(stage1Button.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_stage1", sqButtonSize * 3, sqButtonSize);

		bmp = cast(stage1PresentButton.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_stage1present", sqButtonSize, sqButtonSize);

		BitmapFont.initFont("font_anzu_white", "highscore", Std.int(sqButtonSize * 0.4));
		stage1HighScore.letterSpacing = -sqButtonSize * 0.05;
		stage1HighScore.reposition();


		bmp = cast(stage2Button.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_stage2", sqButtonSize * 3, sqButtonSize);

		bmp = cast(stage2PresentButton.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_stage2present", sqButtonSize, sqButtonSize);

		BitmapFont.initFont("font_anzu_white", "highscore", Std.int(sqButtonSize * 0.4));
		stage2HighScore.letterSpacing = -sqButtonSize * 0.05;
		stage2HighScore.reposition();

		
		bmp = cast(bgmVol25Button.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_vol25", sqButtonSize, sqButtonSize);

		bmp = cast(bgmVol50Button.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_vol50", sqButtonSize, sqButtonSize);

		bmp = cast(bgmVol75Button.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_vol75", sqButtonSize, sqButtonSize);

		bmp = cast(bgmVol100Button.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("menu_vol100", sqButtonSize, sqButtonSize);

		//中央に表示させるためのオフセット
		var oX = 0.0, oY = 0.0;
		if (e.newWidth >= e.newHeight) {
			oX = s * 0.25;
		} else {
			oY = s * 0.25;
		}

		if (e.newWidth >= e.newHeight) {
			backButton.x = (s * 0.25 - sqButtonSize) * 0.5;
			backButton.y = oY + s - sqButtonSize - (s * 0.25 - sqButtonSize) * 0.5;
		} else {
			backButton.x = (s * 0.25 - sqButtonSize) * 0.5;
			backButton.y = oY + s + (s * 0.25 - sqButtonSize) * 0.5;
		}

		//クリア済みの場合はプレゼントへの直通リンク用ボタンを出すために位置計算を変える
		var stageButtonWidth = if (stage1PresentButton.visible || stage2PresentButton.visible) sqButtonSize * 4 else sqButtonSize * 3;
		stage1Button.x = oX + (s - stageButtonWidth) * 0.5;
		stage1Button.y = oY + sqButtonSize * 0.75;
		stage1PresentButton.x = oX + (s -stageButtonWidth) * 0.5 + sqButtonSize * 3;
		stage1PresentButton.y = oY + sqButtonSize * 0.75;
		stage1HighScore.x = oX + (s - stageButtonWidth) * 0.5 + sqButtonSize * 3;
		stage1HighScore.y = oY + sqButtonSize * 0.75 + sqButtonSize + sqButtonSize * 0.2;

		stage2Button.x = oX + (s - stageButtonWidth) * 0.5;
		stage2Button.y = oY + sqButtonSize * 2.5;
		stage2PresentButton.x = oX + (s -stageButtonWidth) * 0.5 + sqButtonSize * 3;
		stage2PresentButton.y = oY + sqButtonSize * 2.5;
		stage2HighScore.x = oX + (s - stageButtonWidth) * 0.5 + sqButtonSize * 3;
		stage2HighScore.y = oY + sqButtonSize * 2.5 + sqButtonSize + sqButtonSize * 0.2;

		bgmVol25Button.x = oX + sqButtonSize * 1;
		bgmVol25Button.y = oY + s - sqButtonSize - (s * 0.25 - sqButtonSize) * 0.5;
		bgmVol50Button.x = oX + sqButtonSize * 2;
		bgmVol50Button.y = oY + s - sqButtonSize - (s * 0.25 - sqButtonSize) * 0.5;
		bgmVol75Button.x = oX + sqButtonSize * 3;
		bgmVol75Button.y = oY + s - sqButtonSize - (s * 0.25 - sqButtonSize) * 0.5;
		bgmVol100Button.x = oX + sqButtonSize * 4;
		bgmVol100Button.y = oY + s - sqButtonSize - (s * 0.25 - sqButtonSize) * 0.5;
	}
}