package ;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Shape;
import nme.display.Sprite;

/**
 * ...
 * @author oov
 */

class TimerGauge extends Sprite
{
	private var gauge: Bitmap;
	private var gaugeMask: Bitmap;
	private var warnMask: Bitmap;
	private var gaugeName: String;
	public var value(default, setValue): Float;
	public var warn(default, setWarn): Float;

	public function new(gaugeName_: String) 
	{
		super();
		gaugeName = gaugeName_;

		gauge = Util.createEmptyBitmap();
		addChild(gauge);
		
		gaugeMask = Util.createEmptyBitmap();
		gaugeMask.alpha = 0.75;
		addChild(gaugeMask);

		warnMask = Util.createEmptyBitmap();
		warnMask.alpha = 0;
		addChild(warnMask);
	}

	public function dispose(): Void {
		gauge.bitmapData.dispose();
		removeChild(gauge);
		gaugeMask.bitmapData.dispose();
		removeChild(gaugeMask);
		warnMask.bitmapData.dispose();
		removeChild(warnMask);
	}

	public function setValue(v: Float): Float {
		v = Math.max(0.0, Math.min(1.0, v));
		gaugeMask.scaleX = 1.0 - v;
		gaugeMask.x = gaugeMask.bitmapData.width * v;
		return value = v;
	}

	public function setWarn(v: Float): Float {
		if (v == warn) return v;
		v = Math.max(0.0, Math.min(1.0, v));
		warnMask.alpha = v * 0.5;
		return warn = v;
	}

	public function resize(sz: Float): Void {
		gauge.bitmapData.dispose();
		gauge.bitmapData = Util.getResizedBitmapData(gaugeName, 680 * sz, 40 * sz);
		
		gaugeMask.bitmapData.dispose();
		gaugeMask.bitmapData = new BitmapData(gauge.bitmapData.width, gauge.bitmapData.height, false, 0x3f3f3f);

		warnMask.bitmapData.dispose();
		warnMask.bitmapData = new BitmapData(gauge.bitmapData.width, gauge.bitmapData.height, false, 0xff0000);

		value = value;
	}
}