package ;
import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.easing.Linear;
import com.eclecticdesignstudio.motion.easing.Sine;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.events.Event;
import nme.display.Shape;
import nme.display.Sprite;

/**
 * ...
 * @author oov
 */

class Transition extends Sprite
{
	public static var HALF = "half";

	private var shapes: Array<Bitmap>;
	private var loading: Bitmap;

	public function new() 
	{
		super();
		mouseChildren = false;
		mouseEnabled = false;

		shapes = [];
		for (i in 0...12*8) {
			var shape: Bitmap = Util.createEmptyBitmap();
			shapes.push(shape);
			addChild(shape);
		}

		loading = Util.createEmptyBitmap();
		loading.alpha = 0;
		addChild(loading);

		var anim: Int = 0;
		Actuate.timer(0.02).onComplete(function():Void {
			Actuate.tween(loading, 0.25, { alpha: 1 } ).delay(0.5).ease(Linear.easeNone);
			
			for (i in 0...12*8) {
				var shape: Bitmap = shapes[i];
				shape.rotation = -60;
				shape.scaleX = 0;
				shape.scaleY = 0;
				Actuate.tween(shape, 0.5, { scaleX: 1, scaleY: 1, rotation: 0 } ).ease(Linear.easeNone).delay(0.05+i * 0.005).onComplete(function(): Void {
					if (--anim == 0) {
						Actuate.timer(0.02).onComplete(function():Void {
							dispatchEvent(new Event(Transition.HALF));
							out();
						});
					}
				});
				++anim;
			}
		});
		addEventListener(Event.RESIZE, resize, false, 0);
		addEventListener(Event.COMPLETE, dispose, false, 0);
	}

	public function dispose(e: Event): Void {
		removeEventListener(Event.RESIZE, resize, false);
		removeEventListener(Event.COMPLETE, dispose, false);
		Actuate.timer(0.02).onComplete(function():Void {
			loading.bitmapData.dispose();
			for (i in 0...numChildren) {
				removeChildAt(0);
			}
			shapes = null;
		});
	}

	private function out(): Void {
		//delay を挟んで処理をが集中し過ぎないように
		Actuate.timer(0.02).onComplete(function():Void {
			var anim: Int = 0;

			loading.alpha = 1;
			Actuate.tween(loading, 0.5, { alpha: 0 } ).ease(Linear.easeNone);

			for (i in 0...12 * 8) {
				var shape: Bitmap = shapes[i];
				shape.rotation = 0;
				shape.scaleX = 1;
				shape.scaleY = 1;
				Actuate.tween(shape, 0.5, { scaleX: 0, scaleY: 0, rotation: 60 } ).ease(Linear.easeNone).delay(0.05+i * 0.005).onComplete(function(): Void {
					if (--anim == 0) {
						Actuate.timer(0.02).onComplete(function():Void {
							dispatchEvent(new Event(Event.COMPLETE));
						});
					}
				});
				anim++;
			}
		});
	}

	private function resize(e: ResizeEvent): Void {
		if (e.newWidth >= e.newHeight) {
			var w: Int = Std.int(e.newWidth / 11.5);
			var h: Int = Std.int(e.newHeight / 7.5);
			var i: Int = 0;
			for (y in 0...8) {
				for (x in 0...12) {
					var shape: Bitmap = shapes[i++];
					if (shape.bitmapData.width != w + 1 || shape.bitmapData.height != h + 1) {
						shape.bitmapData.dispose();
						shape.bitmapData = new BitmapData(w + 1, h + 1, false, 0);
					}
					shape.x = x * w;
					shape.y = y * h;
				}
			}
			if (loading.bitmapData.width != w * 4 || loading.bitmapData.height != Std.int(w * 4 / 5)) {
				loading.bitmapData.dispose();
				loading.bitmapData = Util.getResizedBitmapData("loading", w * 4, Std.int(w * 4 / 5));
			}
			loading.x = e.newWidth - loading.bitmapData.width - w / 2;
			loading.y = e.newHeight - loading.bitmapData.height - w / 2;
		} else {
			var w: Int = Std.int(e.newWidth / 7.5);
			var h: Int = Std.int(e.newHeight / 11.5);
			var i: Int = 0;
			for (y in 0...12) {
				for (x in 0...8) {
					var shape: Bitmap = shapes[i++];
					if (shape.bitmapData.width != w + 1 || shape.bitmapData.height != h + 1) {
						shape.bitmapData.dispose();
						shape.bitmapData = new BitmapData(w + 1, h + 1, false, 0);
					}
					shape.x = x * w;
					shape.y = y * h;
				}
			}
			if (loading.bitmapData.width != h * 4 || loading.bitmapData.height != Std.int(h * 4 / 5)) {
				loading.bitmapData.dispose();
				loading.bitmapData = Util.getResizedBitmapData("loading", h * 4, Std.int(h * 4 / 5));
			}
			loading.x = e.newWidth - loading.bitmapData.width - h / 2;
			loading.y = e.newHeight - loading.bitmapData.height - h / 2;
		}
	}
}