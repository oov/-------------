package ;
import nme.events.Event;

/**
 * ...
 * @author oov
 */

class CompleteSceneEvent extends Event
{
	public static var COMPLETE_SCENE = "complete_scene";
	public var nextScene: String;
	public function new(nextScene_: String, type: String = "complete_scene", bubbles: Bool = false, cancelable: Bool = false)
	{
		super(type, bubbles, cancelable);
		nextScene = nextScene_;
	}
	
}