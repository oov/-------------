package ;
import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.easing.Linear;
import nme.Assets;
import nme.display.Bitmap;
import nme.display.BlendMode;
import nme.display.Shape;
import nme.display.Sprite;
import nme.events.Event;
import nme.media.Sound;
import nme.media.SoundChannel;
import nme.net.SharedObject;

/**
 * ...
 * @author oov
 */

class GameMain extends Scene
{
	private var mainWidth: Int;
	private var mainHeight: Int;

	private var stageId: String;
	private var bgmId: String;
	private var phase: Int;
	private var gamePhase: GamePhase;
	private var gamePhases: Array<GamePhase>;
	private var nextPhaseScore: Int;
	private var totalTime: Int;
	private var nextPhaseEndTime: Int;
	private var eraseTime: Float;
	private var harryUpSEPlay: Int;
	private var warnSEPlay: Int;

	private var harryUpOverlay: Shape;
	private var field: Field;
	private var fieldOuter: Shape;
	private var statusBackground: Bitmap;
	private var status: Sprite;
	private var scoreBoard: Bitmap;
	private var scoreUnit: BitmapFont;
	private var timeUnit: BitmapFont;
	private var presentProgress: PresentProgress;
	private var timerGauge: TimerGauge;

	private var countDown: CountDown;
	
	private var gameOver: GameOver;
	private var gameClear: GameClear;

	/**
	 * 経過時間
	 */
	public var watch: Stopwatch;

	private var score: Int;
	private var lastTime: Int;

	private var countSE: Sound;
	private var startSE: Sound;
	private var moveSE: Sound;
	private var eraseSE: Array<Sound>;
	private var dropSE: Sound;
	private var selectSE: Sound;
	private var warnSE: Sound;

	private var bgm: Sound;
	private var gameOverBGM: Sound;
	private var gameClearBGM: Sound;
	private var bgmChannel: SoundChannel;

	public function new(stageId_: String, bgmId_: String, gamePhases_: Array<GamePhase>)
	{
		super();
		mouseEnabled = false;

		score = 0;
		phase = -1;
		nextPhaseEndTime = 0;
		nextPhaseScore = 0;

		stageId = stageId_;
		bgmId = bgmId_;
		gamePhases = gamePhases_;

		totalTime = 0;
		for (i in 0...gamePhases.length) {
			totalTime += gamePhases[i].seconds;
		}

		//タイルキャッシュの準備
		Tile.initTileBitmapData(1);

		//フィールドの準備
		field = new Field(5, 5);
		addChild(field);

		//タイムアップ間際を知らせるオーバーレイ用シェイプ
		harryUpOverlay = new Shape();
		harryUpOverlay.visible = false;
		harryUpSEPlay = 0;
		warnSEPlay = 0;
		field.backContainer.addChild(harryUpOverlay);


		//余った領域の塗り潰し用
		fieldOuter = new Shape();
		addChild(fieldOuter);

		//カウントダウン
		countDown = new CountDown();
		countDown.addEventListener(CountDown.COUNTDOWN_3, countDownSE);
		countDown.addEventListener(CountDown.COUNTDOWN_2, countDownSE);
		countDown.addEventListener(CountDown.COUNTDOWN_1, countDownSE);
		countDown.addEventListener(CountDown.COUNTDOWN_START, countDownSEStart);
		addChild(countDown);

		//ステータス表示用
		status = new Sprite();
		addChild(status);
		status.mouseEnabled = false;
		status.mouseChildren = false;

		//ステータス表示背景
		statusBackground = Util.createEmptyBitmap();
		status.addChild(statusBackground);

		//スコアボード
		scoreBoard = Util.createEmptyBitmap();
		status.addChild(scoreBoard);

		//スコア
		BitmapFont.initFont("font_anzu_white", "score", 1);
		scoreUnit = new BitmapFont("score");
		scoreUnit.text = "0";
		scoreUnit.align = TextAlign.Right;
		status.addChild(scoreUnit);

		field.addEventListener(Field.SLIDE, fieldOnSlide);
		field.addEventListener(Field.START_CRITICAL, fieldOnStartCritical);
		field.addEventListener(Field.END_CRITICAL, fieldOnEndCritical);
		field.addEventListener(Field.ERASING, fieldOnErasing);
		field.addEventListener(Field.ERASED, fieldOnErased);
		field.addEventListener(Field.DROP, fieldOnDrop);

		//タイム
		BitmapFont.initFont("font_anzu_white", "time", 1);
		timeUnit = new BitmapFont("time");
		timeUnit.text = "0: 00";
		timeUnit.align = TextAlign.Right;
		status.addChild(timeUnit);

		//消す残り時間用ゲージ
		timerGauge =  new TimerGauge("timer_gauge");
		status.addChild(timerGauge);

		//ゲーム達成率表示
		presentProgress = new PresentProgress(stageId + "_present", 0);
		presentProgress.alpha = 0;
		presentProgress.scaleX = 1.1;
		presentProgress.scaleY = 1.1;
		status.addChild(presentProgress);

		watch = new Stopwatch();
		watch.pause();
		watch.step = 0;

		addEventListener(Event.RESIZE, resize);
		addEventListener(Event.ENTER_FRAME, enterFrame);
		addEventListener(PlusHairpin.SOUND_RELOAD, soundReload);

		initSound();
		countDown.addEventListener(CountDown.COUNTDOWN_2, countDown2First);

		restart();
	}

	private function restart(): Void {
		Actuate.timer(0.02).onComplete(function(): Void {
			score = 0;
			phase = -1;
			nextPhaseEndTime = 0;
			nextPhaseScore = 0;
			scoreUnit.text = "0";
			eraseTime = 10;

			presentProgress.reset();

			watch.step = 0;
			watch.unpause();
			
			//特殊駒向けの機能テスト用
/*
			field.chaosMove = Cross;
*/
/*
			Actuate.timer(9).onComplete(function (): Void {
				field.blind = true;
			});
			Actuate.timer(19).onComplete(function (): Void {
				field.blind = false;
			});
*/
/*
			function build(): Void {
				if (field.inputEnable && !field.nowCritical){
					field.buildCombo();
				}
				Actuate.timer(1).onComplete(build);
			}
			Actuate.timer(1).onComplete(build);
*/

			bgmChannel = SoundManager.playMusic(bgm, true);
		});
	}

	private function enterFrame(e: Event): Void {
		//ゲームクリア・ゲームオーバー画面の表示中は何もしない
		if (gameOver != null || gameClear != null || watch.paused) {
			return;
		}

		if (nextPhaseEndTime < watch.step || eraseTime < watch.step) {
			harryUpOverlay.visible = false;
			harryUpSEPlay = 0;
			warnSEPlay = 0;

			if (((phase == gamePhases.length - 1)&&(score >= nextPhaseScore))||((phase != gamePhases.length - 1)&&(nextPhaseEndTime < watch.step))) {
				if (phase == gamePhases.length - 1) {
					watch.pause();
					if (nextPhaseEndTime < watch.step){
						watch.step = nextPhaseEndTime;
					}
					field.inputEnable = false;
					field.drop();
					bgmChannel.stop();
					//スマートフォン上ではローディング処理で一瞬画面が固まるので
					//画面内のアニメーションが全て終了してからゲームクリアシーンを動かし始める
					gameClear = new GameClear();
					Actuate.timer(1.2).onComplete(function(): Void {
						//アプリが落ちてもカバーできるように先に保存する
						var stageClear: SharedObject = SharedObject.getLocal("stageClear");
						var v = Std.int(Reflect.field(stageClear.data, stageId));
						var sc = Std.parseInt(scoreUnit.text);
						if (v < sc){
							Reflect.setField(stageClear.data, stageId, Std.parseInt(scoreUnit.text));
							try {
								stageClear.flush();
							} catch ( e:Dynamic ) {
							}
						}
						gameClear.addEventListener(GameClear.SELECT_RETRY, menuSelectRetry);
						gameClear.addEventListener(GameClear.SELECT_MENU, menuSelectMenu);
						gameClear.addEventListener(GameClear.SELECT_PRESENT, menuSelectPresent);
						gameClear.addEventListener(Event.COMPLETE, gameClearComplete);
						addChild(gameClear);
						gameClear.dispatchEvent(new ResizeEvent(Event.RESIZE, mainWidth, mainHeight));
						bgmChannel = SoundManager.playMusic(gameClearBGM, false);
						gameClear.start();
					});

				} else {
					countDown.addEventListener(CountDown.COUNTDOWN_1, countDown1FieldInit);
					countDown.addEventListener(CountDown.COUNTDOWN_COMPLETE, countDownComplete);
					setupNextPhase();
				}
			} else {
				watch.pause();
				if (nextPhaseEndTime < watch.step){
					watch.step = nextPhaseEndTime;
				}
				field.inputEnable = false;
				field.drop();
				bgmChannel.stop();
				//スマートフォン上ではローディング処理で一瞬画面が固まるので
				//画面内のアニメーションが全て終了してからゲームオーバーシーンを動かし始める
				gameOver = new GameOver();
				Actuate.timer(1.2).onComplete(function(): Void {
					gameOver.addEventListener(GameOver.SELECT_RETRY, menuSelectRetry);
					gameOver.addEventListener(GameOver.SELECT_MENU, menuSelectMenu);
					gameOver.addEventListener(Event.COMPLETE, gameOverComplete);
					addChild(gameOver);
					gameOver.dispatchEvent(new ResizeEvent(Event.RESIZE, mainWidth, mainHeight));
					bgmChannel = SoundManager.playMusic(gameOverBGM, false);
					gameOver.start();
				});
			}
		}
		updateTime();
	}

	private function updateTime(): Void {
		//消す猶予時間の警告
		timerGauge.value = 1.0 - (eraseTime - watch.step) / 10.0;
		if (eraseTime - watch.step < 5.0 && !watch.paused) {
			var f = eraseTime - watch.step;
			var i = Std.int(f);
			if (warnSEPlay != i) {
				SoundManager.playSE(warnSE);
				warnSEPlay = i;
			}
			timerGauge.warn = f - Math.floor(f);
		} else {
			timerGauge.warn = 0;
			warnSEPlay = 0;
		}

		//ゲーム全体の終了の警告
		if (nextPhaseEndTime - watch.step < 10.0 && !watch.paused) {
			var f = nextPhaseEndTime - watch.step;
			var i = Std.int(f);
			if (harryUpSEPlay != i) {
				SoundManager.playSE(countSE);
				harryUpSEPlay = i;
			}
			harryUpOverlay.visible = true;
			harryUpOverlay.alpha = (f - Math.floor(f)) * 0.25;
		} else {
			harryUpOverlay.visible = false;
			harryUpSEPlay = 0;
		}
		
		var t: Int = Std.int(Math.max(0, totalTime - watch.step));
		if (t != lastTime) {
			var secs = Std.int(t % 60);
			timeUnit.text = Std.int(t / 60) + ": " + (if (secs < 10) "0" else "") + secs;
			lastTime = t;
		}
	}

	private function setupNextPhase(): Void {
		//次の phase へ移行
		gamePhase = gamePhases[++phase];
		nextPhaseEndTime += gamePhase.seconds;
		nextPhaseScore = score + gamePhase.score;

		//表示上の時間を合わせる
		watch.pause();
		watch.step = nextPhaseEndTime - gamePhase.seconds;
		eraseTime = watch.step + 10.0;
		updateTime();

		field.inputEnable = false;
		field.drop();
		countDown.nextPlayTime = gamePhase.seconds;
		countDown.nowScore = score;
		countDown.nextTargetScore = nextPhaseScore;
		countDown.visible = true;
		countDown.animate();
	}

	private function countDownSE(e: Event): Void {
		SoundManager.playSE(countSE);
	}

	private function countDownSEStart(e: Event): Void {
		SoundManager.playSE(startSE);
	}

	private function countDown2First(e: Event): Void {
		countDown.removeEventListener(CountDown.COUNTDOWN_2, countDown2First);
		Actuate.tween(presentProgress, 1, {alpha: 1.0, scaleX: 1.0, scaleY: 1.0}, false);
	}

	private function countDown1FieldInit(e: Event): Void {
		countDown.removeEventListener(CountDown.COUNTDOWN_1, countDown1FieldInit);
		field.init(gamePhase.fieldSeed);
	}

	private function countDownComplete(e: Event): Void {
		countDown.removeEventListener(CountDown.COUNTDOWN_COMPLETE, countDownComplete);
		countDown.visible = false;
		field.inputEnable = true;
		watch.unpause();
	}

	private function fieldOnSlide(e: SlideEvent): Void {
		SoundManager.playSE(moveSE);
	}

	private function fieldOnDrop(e: Event): Void {
		SoundManager.playSE(dropSE);
	}

	private function fieldOnStartCritical(e: Event): Void {
		watch.pause();
	}

	private function fieldOnEndCritical(e: Event): Void {
		watch.unpause();
	}

	private function fieldOnErasing(e: EraseEvent): Void {
		SoundManager.playSE(eraseSE[Std.int(Math.min(eraseSE.length-1, e.chain-1))]);
	}

	private function updateScore(s: Int): Void {
		scoreUnit.text = Std.string(s);
	}
	private function fieldOnErased(e: EraseEvent): Void {
		var oldScore: Int = score;
		var newScore: Int = score += Std.int(Math.min(6400, 100 * Math.pow(2, e.chain - 1)));
		Actuate.update(updateScore, 0.25 * e.chain, [oldScore], [newScore]).ease(Linear.easeNone);
		var f: Float = (phase + ((Math.min(nextPhaseScore, score)-(nextPhaseScore-gamePhase.score)) / gamePhase.score)) / gamePhases.length;
		presentProgress.progress = f;
		eraseTime = watch.step + 10.0;
	}

	override function dispose(): Void {
		countSE = null;
		startSE = null;

		warnSE = null;
		moveSE = null;
		eraseSE = [];

		bgm = null;
		bgmChannel = null;
		gameOverBGM = null;
		gameClearBGM = null;

		countDown.removeEventListener(CountDown.COUNTDOWN_3, countDownSE);
		countDown.removeEventListener(CountDown.COUNTDOWN_2, countDownSE);
		countDown.removeEventListener(CountDown.COUNTDOWN_1, countDownSE);
		countDown.removeEventListener(CountDown.COUNTDOWN_START, countDownSEStart);
		countDown.removeEventListener(CountDown.COUNTDOWN_2, countDown2First);
		countDown.removeEventListener(CountDown.COUNTDOWN_1, countDown1FieldInit);
		countDown.removeEventListener(CountDown.COUNTDOWN_COMPLETE, countDownComplete);
		countDown.dispose();
		countDown = null;

		field.removeEventListener(Field.SLIDE, fieldOnSlide);
		field.removeEventListener(Field.START_CRITICAL, fieldOnStartCritical);
		field.removeEventListener(Field.END_CRITICAL, fieldOnEndCritical);
		field.removeEventListener(Field.ERASING, fieldOnErasing);
		field.removeEventListener(Field.ERASED, fieldOnErased);
		field.removeEventListener(Field.DROP, fieldOnDrop);

		removeEventListener(Event.RESIZE, resize);
		removeEventListener(Event.ENTER_FRAME, enterFrame);
		removeEventListener(PlusHairpin.SOUND_RELOAD, soundReload);

		status.removeChild(presentProgress);
		presentProgress.dispose();
		presentProgress = null;

		status.removeChild(timerGauge);
		timerGauge.dispose();

		status.removeChild(timeUnit);
		BitmapFont.disposeFont("time");
		timeUnit = null;

		status.removeChild(scoreUnit);
		BitmapFont.disposeFont("score");
		scoreUnit = null;

		scoreBoard.bitmapData.dispose();
		status.removeChild(scoreBoard);
		scoreBoard = null;

		statusBackground.bitmapData.dispose();
		status.removeChild(statusBackground);
		statusBackground = null;

		removeChild(status);

		removeChild(fieldOuter);
		fieldOuter = null;

		field.backContainer.removeChild(harryUpOverlay);
		field.dispose();
		removeChild(field);
		field = null;
	}

	private function initSound(): Void {
		if (bgm == null){
			bgm = Assets.getSound(bgmId);
		}
		if (gameOverBGM == null){
			gameOverBGM = Assets.getSound("gameover");
		}
		if (gameClearBGM == null){
			gameClearBGM = Assets.getSound("gameclear");
		}

		warnSE = Assets.getSound("warn");
		countSE = Assets.getSound("count");
		startSE = Assets.getSound("start");

		moveSE = Assets.getSound("move");

		eraseSE = [];
		for (i in 1...9) {
			eraseSE.push(Assets.getSound("erase"+i));
		}
		
		dropSE = Assets.getSound("drop");
		
		selectSE = Assets.getSound("select");
	}

	private function soundReload(e: Event): Void {
		initSound();
	}

	private function resize(e: ResizeEvent): Void {
		mainWidth = e.newWidth;
		mainHeight = e.newHeight;

		var tileSize: Int = Std.int(if (e.newWidth < e.newHeight) e.newWidth / field.fieldWidth else e.newHeight / field.fieldHeight);

		harryUpOverlay.graphics.clear();
		harryUpOverlay.graphics.beginFill(0xff0000);
		harryUpOverlay.graphics.drawRect(0, 0, tileSize * field.fieldWidth, tileSize * field.fieldHeight);
		harryUpOverlay.graphics.endFill();

		Tile.initTileBitmapData(tileSize);
		field.reposition(tileSize);
		fieldOuter.graphics.clear();

		var bdsz = tileSize * 2.1;
		if (scoreBoard.bitmapData.width != bdsz) {
			scoreBoard.bitmapData.dispose();
			scoreBoard.bitmapData = Util.getResizedBitmapData("scoreboard", bdsz, bdsz);
		}
		scoreBoard.x = tileSize * 0.22;
		scoreBoard.y = tileSize * 0.35;
		
		timerGauge.resize(tileSize * 0.00325);
		timerGauge.x = tileSize * 0.17;
		timerGauge.y = tileSize * 0.1;

		if (e.newWidth > e.newHeight) {
			if (field.fieldHeight * tileSize < e.newHeight) {
				fieldOuter.graphics.beginFill(0);
				fieldOuter.graphics.drawRect(0, field.fieldHeight * tileSize, e.newWidth, e.newHeight - field.fieldHeight * tileSize);
				fieldOuter.graphics.endFill();
			}

			var scale: Float = e.newWidth - e.newHeight;
			status.x = field.fieldWidth * tileSize;
			status.y = 0;
			if (statusBackground.bitmapData.width != Std.int(scale) || statusBackground.height != e.newHeight) {
				statusBackground.bitmapData.dispose();
				statusBackground.bitmapData = Util.getResizedBitmapData("statusH", Std.int(scale), field.fieldHeight * tileSize);
			}

			BitmapFont.initFont("font_anzu_white", "score", Std.int(scale * 0.17));
			scoreUnit.letterSpacing = scale * -0.02;
			scoreUnit.reposition();
			scoreUnit.x = scale * 0.88;
			scoreUnit.y = scale * 0.80;

			BitmapFont.initFont("font_anzu_white", "time", Std.int(scale * 0.17));
			timeUnit.letterSpacing = scale * -0.02;
			timeUnit.reposition();
			timeUnit.x = scale * 0.88;
			timeUnit.y = scale * 0.42;

			presentProgress.resize(Std.int(scale * 0.6));
			presentProgress.x = scale * 0.05;
			presentProgress.y = scale * 1.35;
			
			countDown.resize(Std.int(e.newHeight));
		} else {
			if (field.fieldWidth * tileSize < e.newWidth) {
				fieldOuter.graphics.beginFill(0);
				fieldOuter.graphics.drawRect(field.fieldWidth * tileSize, 0, e.newWidth - field.fieldWidth * tileSize, e.newHeight);
				fieldOuter.graphics.endFill();
			}

			var scale: Float = e.newHeight - e.newWidth;
			status.x = 0;
			status.y = field.fieldHeight * tileSize;
			
			if (statusBackground.bitmapData.width != e.newWidth || statusBackground.height != Std.int(scale)) {
				statusBackground.bitmapData.dispose();
				statusBackground.bitmapData = Util.getResizedBitmapData("statusV", field.fieldWidth * tileSize, Std.int(scale));
			}

			BitmapFont.initFont("font_anzu_white", "score", Std.int(scale * 0.17));
			scoreUnit.letterSpacing = scale * -0.02;
			scoreUnit.reposition();
			scoreUnit.x = scale * 0.88;
			scoreUnit.y = scale * 0.80;

			BitmapFont.initFont("font_anzu_white", "time", Std.int(scale * 0.17));
			timeUnit.letterSpacing = scale * -0.02;
			timeUnit.reposition();
			timeUnit.x = scale * 0.88;
			timeUnit.y = scale * 0.42;

			presentProgress.resize(Std.int(scale * 0.6));
			presentProgress.x = scale * 1.35;
			presentProgress.y = scale * 0.05;

			countDown.resize(Std.int(e.newWidth));
		}
		
		if (gameOver != null) {
			gameOver.dispatchEvent(new ResizeEvent(Event.RESIZE, e.newWidth, e.newHeight));
		}
		if (gameClear != null) {
			gameClear.dispatchEvent(new ResizeEvent(Event.RESIZE, e.newWidth, e.newHeight));
		}
	}
	
	private function menuSelectRetry(e: Event): Void {
		SoundManager.playSE(selectSE);
		Actuate.timer(1.25).onComplete(function(): Void {
			restart();
		});
	}

	private function menuSelectMenu(e: Event): Void {
		SoundManager.playSE(selectSE);
		Actuate.timer(1.25).onComplete(function(): Void {
			dispatchEvent(new CompleteSceneEvent("menu"));
		});
	}

	private function menuSelectPresent(e: Event): Void {
		Util.getURL(Util.buildPresentURL(stageId, Std.parseInt(scoreUnit.text)));
	}

	private function gameOverComplete(e: Event): Void {
		gameOver.removeEventListener(GameOver.SELECT_RETRY, menuSelectRetry);
		gameOver.removeEventListener(GameOver.SELECT_MENU, menuSelectMenu);
		gameOver.removeEventListener(Event.COMPLETE, gameOverComplete);
		removeChild(gameOver);
		gameOver.dispose();
		gameOver = null;
	}
	
	private function gameClearComplete(e: Event): Void {
		gameClear.removeEventListener(GameClear.SELECT_RETRY, menuSelectPresent);
		gameClear.removeEventListener(GameClear.SELECT_RETRY, menuSelectRetry);
		gameClear.removeEventListener(GameClear.SELECT_MENU, menuSelectMenu);
		gameClear.removeEventListener(Event.COMPLETE, gameClearComplete);
		removeChild(gameClear);
		gameClear.dispose();
		gameClear = null;
	}
}