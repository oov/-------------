package ;
import nme.events.Event;
import nme.media.Sound;
import nme.media.SoundChannel;
import nme.media.SoundTransform;
import nme.net.SharedObject;

/**
 * ...
 * @author oov
 */

class SoundManager 
{
	public static var globalVolume(default, setGlobalVolume): Float;
	public static var mode(default,setMode): SoundMode;
	
	private static var bgm: SoundChannel;
	private static var se: Array<SoundChannel> = [];

	public static function setGlobalVolume(v: Float): Float {
		globalVolume = v;
		mode = mode;

		var soundSetting: SharedObject = SharedObject.getLocal("sound-setting");
		soundSetting.data.globalVolume = v;
		try {
			soundSetting.flush();
		} catch ( e:Dynamic ) {
		}

		return v;
	}

	public static function restoreGlobalVolume(): Void {
		var soundSetting: SharedObject = SharedObject.getLocal("sound-setting");
		var vol = Std.parseFloat(soundSetting.data.globalVolume);
		if (soundSetting.data.globalVolume == null || vol < 0.1 || Math.isNaN(vol)) {
			vol = 0.5;
		}
		globalVolume = vol;
	}

	public static function setMode(v: SoundMode): SoundMode {
		switch(v) {
			case SoundMode.MusicOn:
				if (bgm != null){
					bgm.soundTransform = new SoundTransform(globalVolume);
				}
				for (i in 0...se.length) {
					se[i].soundTransform = new SoundTransform(globalVolume);
				}
			case SoundMode.SeOnly:
				if (bgm != null){
					bgm.soundTransform = new SoundTransform(0);
				}
				for (i in 0...se.length) {
					se[i].soundTransform = new SoundTransform(globalVolume);
				}
			case SoundMode.Mute:
				if (bgm != null){
					bgm.soundTransform = new SoundTransform(0);
				}
				for (i in 0...se.length) {
					se[i].soundTransform = new SoundTransform(0);
				}
		}
		return mode = v;
	}

	public static function playSE(sound: Sound): SoundChannel {
		var st: SoundTransform = new SoundTransform(globalVolume);
		if (mode == SoundMode.Mute) {
			st.volume = 0;
		}
		var sc: SoundChannel = sound.play(0, 0, st);
		se.push(sc);
		sc.addEventListener(Event.SOUND_COMPLETE, function(e: Event): Void {
			for (i in 0...se.length) {
				if (se[i] == sc) {
					se.splice(i, 1);
					break;
				}
			}
		});
		return sc;
	}

	public static function playMusic(sound: Sound, loop: Bool): SoundChannel {
		if (bgm != null) {
			bgm.stop();
		}
		var st: SoundTransform = new SoundTransform(globalVolume);
		if (mode != SoundMode.MusicOn) {
			st.volume = 0;
		}
		var loops: Int = 0;
		if (loop){
			#if flash
				loops = 0x3fffffff;
			#else
				loops = -1;
			#end
		}
		return bgm = sound.play(0, loops, st);
	}
	
	public static function stopMusic(): Void {
		if (bgm != null) {
			bgm.stop();
			bgm = null;
		}
	}
}