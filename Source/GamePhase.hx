package ;

/**
 * フェーズのクリア条件
 * @author oov
 */

class GamePhase 
{
	public var score: Int;
	public var seconds: Int;
	public var lines: Int;
	public var chains: Int;
	public var fieldSeed: Int;

	public function new() {
		score = 0;
		seconds = 0;
		lines = 0;
		chains = 0;
		fieldSeed = 0;
	}
}