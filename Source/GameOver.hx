package ;
import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.easing.Linear;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.MouseEvent;

/**
 * ...
 * @author oov
 */

class GameOver extends Sprite
{
	public static var SELECT_RETRY = "select_retry";
	public static var SELECT_MENU = "select_menu";

	private var background: Bitmap;
	private var boardContainer: Sprite;
	private var board: Bitmap;
	private var menuButton: Sprite;
	private var retryButton: Sprite;
	
	private var clicked: Bool;

	public function new() 
	{
		super();

		mouseEnabled = false;
		clicked = false;

		background = Util.createEmptyBitmap();
		background.alpha = 0;
		addChild(background);

		boardContainer = new Sprite();
		boardContainer.alpha = 0;
		addChild(boardContainer);
		
		board = Util.createEmptyBitmap();
		boardContainer.addChild(board);

		menuButton = new Sprite();
		menuButton.addChild(Util.createEmptyBitmap());
		menuButton.alpha = 0;
		menuButton.addEventListener(MouseEvent.MOUSE_DOWN, menuClick);
		boardContainer.addChild(menuButton);

		retryButton = new Sprite();
		retryButton.addChild(Util.createEmptyBitmap());
		retryButton.alpha = 0;
		retryButton.addEventListener(MouseEvent.MOUSE_DOWN, retryClick);
		boardContainer.addChild(retryButton);

		addEventListener(Event.RESIZE, resize);
	}
	
	public function dispose(): Void {
		removeEventListener(Event.RESIZE, resize);

		retryButton.removeEventListener(MouseEvent.CLICK, retryClick);
		cast(retryButton.getChildAt(0), Bitmap).bitmapData.dispose();
		boardContainer.removeChild(retryButton);

		menuButton.removeEventListener(MouseEvent.CLICK, menuClick);
		cast(menuButton.getChildAt(0), Bitmap).bitmapData.dispose();
		boardContainer.removeChild(menuButton);

		board.bitmapData.dispose();
		boardContainer.removeChild(board);
		
		removeChild(boardContainer);

		background.bitmapData.dispose();
		removeChild(background);
	}
	
	public function menuClick(e: MouseEvent): Void {
		if (clicked) {
			return;
		}

		dispatchEvent(new Event(SELECT_MENU));
		clicked = true;
		
		var bmp: Bitmap = Util.createBitmap(cast(menuButton.getChildAt(0), Bitmap).bitmapData);
		boardContainer.addChild(bmp);
		bmp.x = menuButton.x;
		bmp.y = menuButton.y;
		Actuate.update(function(v: Float, a: Float): Void {
			bmp.x = menuButton.x - v * bmp.bitmapData.width * 0.5;
			bmp.y = menuButton.y - v * bmp.bitmapData.height * 0.5;
			bmp.scaleX = 1 + v;
			bmp.scaleY = 1 + v;
			bmp.alpha = a;
			retryButton.alpha = a;
		}, 0.5, [0, 1], [0.5, 0]).onComplete(function(): Void {
			boardContainer.removeChild(bmp);
			end();
		});
	}

	public function retryClick(e: MouseEvent): Void {
		if (clicked) {
			return;
		}

		dispatchEvent(new Event(SELECT_RETRY));
		clicked = true;
		
		var bmp: Bitmap = Util.createBitmap(cast(retryButton.getChildAt(0), Bitmap).bitmapData);
		boardContainer.addChild(bmp);
		bmp.x = retryButton.x;
		bmp.y = retryButton.y;
		Actuate.update(function(v: Float, a: Float): Void {
			bmp.x = retryButton.x - v * bmp.bitmapData.width * 0.5;
			bmp.y = retryButton.y - v * bmp.bitmapData.height * 0.5;
			bmp.scaleX = 1 + v;
			bmp.scaleY = 1 + v;
			bmp.alpha = a;
			menuButton.alpha = a;
		}, 0.5, [0, 1], [0.5, 0]).onComplete(function(): Void {
			boardContainer.removeChild(bmp);
			end();
		});
	}

	public function resize(e: ResizeEvent): Void {
		background.bitmapData.dispose();
		background.bitmapData = new BitmapData(e.newWidth, e.newHeight, false, 0);

		var s: Int = Std.int(Math.min(e.newWidth, e.newHeight));
		boardContainer.x = (e.newWidth - s) / 2;
		boardContainer.y = (e.newHeight - s) / 2;

		board.bitmapData.dispose();
		board.bitmapData = Util.getResizedBitmapData("gameover_board", s, s);

		var sc: Float = s * 0.038;
		var w: Int = Std.int(sc * 5);
		var h: Int = Std.int(sc * 6);

		var bmp: Bitmap;
		bmp = cast(menuButton.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("gameover_menu", w, h);
		menuButton.x = (s - w) / 2 + sc * 3.75;
		menuButton.y = sc * 16.8;

		bmp = cast(retryButton.getChildAt(0), Bitmap);
		bmp.bitmapData.dispose();
		bmp.bitmapData = Util.getResizedBitmapData("gameover_retry", w, h);
		retryButton.x = (s - w) / 2 - sc * 3.75;
		retryButton.y = sc * 16.8;
	}
	
	public function start(): Void {
		Actuate.tween(background, 0.5, {alpha: 0.6}).ease(Linear.easeNone);
		Actuate.tween(menuButton, 0.75, { alpha: 1 } ).delay(0.9);
		Actuate.tween(retryButton, 0.75, { alpha: 1 } ).delay(0.8);
		Actuate.update(function(y: Float): Void {
			boardContainer.y = (background.bitmapData.height - board.bitmapData.height) * 0.5 + board.bitmapData.height * y * 0.1;
			boardContainer.alpha = 1 - y;
		}, 0.5, [1], [0]).delay(0.5);
	}
	
	public function end(): Void {
		Actuate.update(function(y: Float): Void {
			boardContainer.alpha = 1 - y;
		}, 0.5, [0], [1]);
		Actuate.tween(background, 0.5, { alpha: 0 } ).ease(Linear.easeNone).delay(0.25).onComplete(function(): Void {
			dispatchEvent(new Event(Event.COMPLETE));
		});
	}
}