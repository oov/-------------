package ;

/**
 * ...
 * @author oov
 */

enum SlideTo {
	Up(x: Int);
	Down(x: Int);
	Left(y: Int);
	Right(y: Int);
}