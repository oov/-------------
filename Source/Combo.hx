package ;

/**
 * ...
 * @author oov
 */

class Combo
{
	private static function rotField(x: Int, y: Int, width: Int, height: Int, rot: Int): Int {
		if (width != height) {
			//縦横比が違う場合はデータがぶっ壊れるので反転系だけにしておく
			rot = rot % 4;
		}
		switch(rot) {
			case 0: //そのまま
				return x + y * width;
			case 1: //左右反転
				return width - x - 1 + y * width;
			case 2: //上下反転
				return x + (height - y - 1) * width;
			case 3: //上下左右反転
				return width - x - 1 + (height - y - 1) * width;
			case 4: //90°回転
				return x * width + width - y - 1;
			case 5: //180°回転
				return width - x - 1 + (width - y - 1) * width;
			case 6: //270°回転
				return y + (width - x - 1) * width;
			default:
				return 0;
		}
	}

	private static function getRandomCombo5x5(r: XorshiftRNG): Array<Int> {
		switch(Std.int(r.next() * 6)) {
			case 0:
				return [
					0, 0, 0, 0, 0,
					1, 1, 1, 1, 1,
					2, 2, 2, 2, 2,
					3, 3, 3, 3, 3,
					4, 4, 4, 4, 4,
				];
			case 1:
				return [
					0, 0, 0, 0, 0,
					1, 2, 2, 2, 2,
					1, 3, 4, 4, 4,
					1, 3, 5, 6, 6,
					1, 3, 5, 7, 7,
				];
			case 2:
				return [
					0, 0, 0, 0, 0,
					1, 3, 3, 3, 2,
					1, 5, 6, 7, 2,
					1, 5, 6, 7, 2,
					1, 4, 4, 4, 2,
				];
			case 3:
				return [
					0, 0, 0, 0, 0,
					1, 1, 1, 1, 1,
					2, 2, 2, 2, 2,
					3, 4, 5, 7, 7,
					3, 4, 5, 6, 6,
				];
			case 4:
				return [
					0, 0, 0, 0, 0,
					1, 1, 1, 1, 1,
					3, 4, 5, 5, 5,
					3, 4, 6, 6, 6,
					2, 2, 2, 2, 2,
				];
			case 5:
				return [
					0, 0, 0, 0, 0,
					1, 3, 3, 3, 3,
					1, 4, 5, 5, 5,
					1, 4, 6, 6, 6,
					1, 2, 2, 2, 2,
				];
			default:
				return [
					0, 0, 0, 0, 0,
					0, 0, 0, 0, 0,
					0, 0, 0, 0, 0,
					0, 0, 0, 0, 0,
					0, 0, 0, 0, 0,
				];
		}
	}
	public static function get(rand: XorshiftRNG, colors: Int, width: Int, height: Int): FieldArray<Tile> {
		var combo: Array<Int> = [];
		var r = new FieldArray<Tile>(width, height, colors, null);
		switch(width) {
			case 5:
				switch(height) {
					case 5:
						combo = getRandomCombo5x5(rand);
					default:
						combo = [];
				}
			default:
				combo = [];
		}
		
		var rot = Std.int(rand.next() * 7);
		var color = Std.int(rand.next() * colors);
		for (y in 0...height) {
			for (x in 0...width) {
				r.setRaw(
					rotField(x, y, width, height, rot),
					new Tile((color + combo[x + y * width]) % colors)
				);
			}
		}
		return r;
	}
}