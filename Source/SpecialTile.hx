package ;
import haxe.Timer;

/**
 * ...
 * @author oov
 */

class SpecialTile extends Tile
{
	public function new(color_: Int) {
		super(color_);
		animated = true;
	}

	override function step(elapsed:Int)
	{
		rotation = (Timer.stamp()) % 360;
	}
}