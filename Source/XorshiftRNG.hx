package ;

/**
 * Xorshift 乱数生成器
 * @author oov
 */

class XorshiftRNG 
{
	private static inline function mul(a: Int, b: Int): Int {
		var a1: Int = a >>> 16, a2: Int = a & 0xffff;
		var b1: Int = b >>> 16, b2: Int = b & 0xffff;
		return ((a1 * b2 + a2 * b1) << 16) + a2 * b2;
	}

	private static inline function toUInt32(a: Int): Float {
		return (a & 0xffff) + ((a >>> 16) & 0xffff) * 65536.0;		
	}

	private var x: Int;
	private var y: Int;
	private var z: Int;
	private var w: Int;

	public function new(?seed: Int) {
		if (seed == null) {
			setSeedByTime();
		} else {
			setSeed(seed);
		}
	}
	
	public function setSeedByTime(): Void {
		setSeed(Std.int(Date.now().getTime()));
	}
	
	public function setSeed(seed: Int): Void {
		var _: Int = 1812433253;
		x = seed = mul(_, seed ^ (seed >>> 30)) + 1;
		y = seed = mul(_, seed ^ (seed >>> 30)) + 2;
		z = seed = mul(_, seed ^ (seed >>> 30)) + 3;
		w = seed = mul(_, seed ^ (seed >>> 30)) + 4;
	}
	
	public function next(): Float {
		return nextUInt32() / 4294967295.0;
	}

	public function nextUInt32(): Float {
		var w_: Int = w;
		var t: Int = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w_ ^ w_ >>> 19) ^ (t ^ t >>> 8);
		return toUInt32(w);
	}
}