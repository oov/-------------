package ;
import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.easing.Cubic;
import com.eclecticdesignstudio.motion.easing.Linear;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.events.Event;

/**
 * ...
 * @author oov
 */

class CountDown extends Sprite
{
	public static var COUNTDOWN_3 = "countdown_3";
	public static var COUNTDOWN_2 = "countdown_2";
	public static var COUNTDOWN_1 = "countdown_1";
	public static var COUNTDOWN_START = "countdown_start";
	public static var COUNTDOWN_COMPLETE = "countdown_complete";

	public var nowScore: Int;
	public var nextTargetScore: Int;
	public var nextPlayTime: Int;

	private var phaseinfo: Sprite;
	private var phaseinfoBackground: Bitmap;
	private var scoreText: BitmapFont;
	private var timeText: BitmapFont;
	private var count: Array<Bitmap>;
	private var start: Bitmap;
	private var needle: Int;

	public function new() {
		super();
		mouseEnabled = false;
		mouseChildren = false;

		count = [
			Util.createEmptyBitmap(),
			Util.createEmptyBitmap(),
			Util.createEmptyBitmap()
		];
		for (i in 0...3) {
			var bmp: Bitmap = count[i];
			bmp.visible = false;
			addChild(bmp);
		}
		start = Util.createEmptyBitmap();
		start.visible = false;
		addChild(start);

		phaseinfo = new Sprite();
		phaseinfo.visible = false;
		addChild(phaseinfo);

		phaseinfoBackground = Util.createEmptyBitmap();
		phaseinfo.addChild(phaseinfoBackground);
		
		BitmapFont.initFont("font_anzu_white", "phaseinfo", 1);
		scoreText = new BitmapFont("phaseinfo");
		scoreText.text = "0";
		scoreText.align = TextAlign.Right;
		phaseinfo.addChild(scoreText);
		timeText = new BitmapFont("phaseinfo");
		timeText.text = "0";
		timeText.align = TextAlign.Right;
		phaseinfo.addChild(timeText);
	}

	public function dispose(): Void {
		for (i in 0...3) {
			var bmp: Bitmap = count[i];
			bmp.bitmapData.dispose();
			removeChild(bmp);
		}
		count = [];
		removeChild(start);
		start.bitmapData.dispose();

		phaseinfo.removeChild(phaseinfoBackground);
		phaseinfoBackground.bitmapData.dispose();
		BitmapFont.disposeFont("phaseinfo");

		removeChild(phaseinfo);
	}

	public function animate(): Void {
		//Phase情報の登場・退場のアニメーション
		phaseinfo.visible = false;
		phaseinfo.alpha = 1;
		Actuate.update(function(a: Float, na: Float): Void {
			timeText.visible = true;
			scoreText.visible = true;
			phaseinfo.visible = true;
			phaseinfo.y = needle * -na;
			phaseinfo.rotation = -5 * na;
		}, 1, [0, 4], [1, 0], false).ease(Cubic.easeOut).onComplete(function(): Void {
			Actuate.update(function(a: Float, na: Float): Void {
				timeText.visible = false;
				scoreText.visible = false;
				phaseinfo.alpha = a;
				phaseinfo.y = needle * na;
				phaseinfo.rotation = 5 * na;
			}, 0.5, [1, 0], [0, 0.5], false).delay(2.5).ease(Cubic.easeOut).onComplete(function(): Void {
				phaseinfo.visible = false;
			});
		});

		//数字類のアニメーション
		scoreText.text = Std.string(nowScore);
		timeText.text = "0: 00";
		Actuate.update(function(score: Float, time: Float): Void {
			scoreText.text = Std.string(Std.int(score));
			var t: Int = Std.int(time);
			var secs: Int = Std.int(t % 60);
			timeText.text = Std.int(t / 60) + ": " + (if (secs < 10) "0" else "") + secs;
		}, 0.5, [nowScore, 0], [nextTargetScore, nextPlayTime], false).delay(1).ease(Linear.easeNone);

		Actuate.timer(4).onComplete(function(): Void {
			//カウントダウン
			for (i in 0...3) {
				(function(bmp: Bitmap): Void {
					bmp.visible = false;
					Actuate.update(function(a: Float): Void {
						bmp.visible = true;
						bmp.alpha = a;
					}, 1, [1], [0], false).ease(Cubic.easeIn).delay(i).onComplete(function(): Void {
						bmp.visible = false;
					});
				})(count[2-i]);
			}
			
			//START が右から左にスライドする処理
			start.visible = false;
			start.x = needle * 9;
			Actuate.update(function(a: Float): Void {
				start.visible = true;
				start.x = needle * 5 * a - needle;
			}, 0.25, [1], [0], false).delay(2.75).ease(Cubic.easeOut).onComplete(function(): Void {
				Actuate.update(function(a: Float): Void {
					start.x = needle * -5 * a - needle;
				}, 0.25, [0], [1], false).delay(0.75).ease(Cubic.easeIn).onComplete(function(): Void {
					start.visible = false;
				});
			});
			dispatchEvent(new Event(CountDown.COUNTDOWN_3));
			Actuate.timer(1).onComplete(function(): Void {
				dispatchEvent(new Event(CountDown.COUNTDOWN_2));
			});
			Actuate.timer(2).onComplete(function(): Void {
				dispatchEvent(new Event(CountDown.COUNTDOWN_1));
			});
			Actuate.timer(3).onComplete(function(): Void {
				dispatchEvent(new Event(CountDown.COUNTDOWN_START));
			});
			Actuate.timer(4).onComplete(function(): Void {
				dispatchEvent(new Event(CountDown.COUNTDOWN_COMPLETE));
			});
		});
	}

	public function resize(sz: Int): Void {
		if (count[0].bitmapData.width == sz >> 2) {
			return;
		}

		needle = sz >> 2;

		for (i in 0...3) {
			count[i].bitmapData.dispose();
			count[i].bitmapData = Util.getResizedBitmapData("count" + (i + 1), needle, needle);
			count[i].x = (sz - needle) * 0.5;
			count[i].y = (sz - needle) * 0.5;
		}
		
		start.bitmapData.dispose();
		start.bitmapData = Util.getResizedBitmapData("countstart", needle + sz, needle >> 1);
		start.y = (sz - (needle >> 1)) / 2;
		
		phaseinfoBackground.bitmapData.dispose();
		phaseinfoBackground.bitmapData = Util.getResizedBitmapData("phaseinfo", sz, sz);
		
		BitmapFont.initFont("font_anzu_white", "phaseinfo", Std.int(sz * 0.13));
		scoreText.letterSpacing = -sz * 0.02;
		scoreText.reposition();
		scoreText.x = sz * 0.9;
		scoreText.y = sz * 0.43;
		timeText.letterSpacing = -sz * 0.02;
		timeText.reposition();
		timeText.x = sz * 0.9;
		timeText.y = sz * 0.68;
	}
}