package ;
import aze.display.TileGroup;
import aze.display.TileLayer;
import aze.display.TileSprite;
import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.actuators.GenericActuator;
import com.eclecticdesignstudio.motion.easing.Back;
import com.eclecticdesignstudio.motion.easing.Cubic;
import com.eclecticdesignstudio.motion.easing.IEasing;
import com.eclecticdesignstudio.motion.easing.Linear;
import com.eclecticdesignstudio.motion.easing.Sine;
import haxe.Timer;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.geom.Point;
import nme.Lib;

private enum Line {
	Horizontal(y: Int);
	Vertical(x: Int);
}

/**
 * ...
 * @author oov
 */

class Field extends Sprite
{
	/**
	 * ユーザーの入力を受け付けられる状態になった時のイベント識別子
	 */
	public static var READY = "ready";

	/**
	 * スライドした時のイベント識別子
	 */
	public static var SLIDE = "slide";

	/**
	 * アニメーションで操作を受け付けなくなった時のイベント識別子
	 */
	public static var START_CRITICAL = "start_critical";
	
	/**
	 * 1列が消える時のイベント識別子
	 */
	public static var ERASING = "erasing";

	/**
	 * 1列が消えた時のイベント識別子
	 */
	public static var ERASED = "erased";

	/**
	 * アニメーションが終了し操作を受け付けられるようになった時のイベント識別子
	 */
	public static var END_CRITICAL = "end_critical";

	/**
	 * 1列が消えた時のイベント識別子
	 */
	public static var DROP = "drop";

	/**
	 * ゲームのプレイが許可されているか
	 */
	public var inputEnable: Bool;

	/**
	 * タイル設置用乱数生成器
	 */
	private var rand: XorshiftRNG;
	
	/**
	 * アニメーション中のオブジェクト数のカウント
	 */
	private var busy(default, setBusy): Int;

	/**
	 * 連鎖数
	 */
	public var chain(default, null): Int;

	/**
	 * 盤面
	 */
	private var field: FieldArray<Tile>;

	public var fieldWidth(getFieldWidth, null): Int;
	public var fieldHeight(getFieldHeight, null): Int;
	
	/**
	 * タイル1枚の辺の大きさ
	 */
	public var tileSize(default,null): Float;
	
	/**
	 * タイル1枚の辺の大きさの半分
	 */
	public var tileHalfSize(default,null): Float;

	public var backContainer: Sprite;
	private var fieldBackground(default,null): Bitmap;
	private var fieldTileLayer: TileLayer;

	/**
	 * タイルの親
	 */
	private var fieldContainer: TileGroup;
	
	/**
	 * 消した時とかのエフェクトの親
	 */
	private var effectContainer: TileGroup;
	
	/**
	 * 目隠し用スプライト
	 */
	private var blindSprite: TileSprite;

	/**
	 * 前回のマウス座標（タイル基準）
	 */
	private var oldMouseTilePos: IntPoint;

	/**
	 * 現在入力中かどうか
	 */
	private var inputActive: Bool;

	/**
	 * 他の処理が走っているので入力が受け付けられないかどうか
	 */
	public var nowCritical(default, null): Bool;
	
	/**
	 * アニメーションが終わったらタイルを消す処理をするかどうか
	 */
	private var nextCheck: Bool;

	/**
	 * 特殊駒の効果による移動反応の変化
	 */
	public var chaosMove: ChaosMove;

	/**
	 * 特殊駒の効果による目隠しモード
	 */
	public var blind(default, setBlind): Bool;
	
	/**
	 * 
	 * @param	width_ 横のマス数
	 * @param	height_ 縦のマス数
	 * @param	tileSize_ タイル1枚の大きさ
	 */
	public function new(width_: Int, height_: Int) {
		super();
		mouseChildren = false;

		//フィールドの背景
		backContainer = new Sprite();
		addChild(backContainer);
		
		fieldBackground = Util.createEmptyBitmap();
		backContainer.addChild(fieldBackground);

		fieldTileLayer = new TileLayer(Tile.sheet);
		addChild(fieldTileLayer.view);

		fieldContainer = new TileGroup();
		fieldTileLayer.addChild(fieldContainer);

		effectContainer = new TileGroup();
		fieldTileLayer.addChild(effectContainer);

		blindSprite = new TileSprite("blind");
		blindSprite.visible = false;
		fieldTileLayer.addChild(blindSprite);

		rand = new XorshiftRNG(0);
		
		busy = 0;
		inputEnable = true;
		inputActive = false;
		chaosMove = Normal;
		oldMouseTilePos = new IntPoint(0, 0);

		tileSize = 2;
		tileHalfSize = 1;
		field = new FieldArray<Tile>(width_, height_, 5, null);

		nextCheck = false;

		chain = 0;

		addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
		addEventListener(MouseEvent.MOUSE_UP, mouseUp);
		addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
		addEventListener(Event.ENTER_FRAME, enterFrame);
		addEventListener(Field.READY, ready);
	}

	public function dispose(): Void {
		removeEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
		removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		removeEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
		removeEventListener(Event.ENTER_FRAME, enterFrame);
		removeEventListener(Field.READY, ready);

		fieldTileLayer.removeChild(blindSprite);
		blindSprite = null;

		fieldTileLayer.removeChild(effectContainer);
		effectContainer = null;

		for (y in 0...field.height) {
			for (x in 0...field.width) {
				set(null, x, y);
			}
		}
		field = null;
		fieldTileLayer.removeChild(fieldContainer);
		fieldContainer = null;
		
		removeChild(fieldTileLayer.view);

		fieldBackground.bitmapData.dispose();
		backContainer.removeChild(fieldBackground);
		fieldBackground = null;
		
		removeChild(backContainer);
	}

	public function enterFrame(e: Event): Void {
		if (blindSprite.visible) {
			blindSprite.rotation = Math.sin(Timer.stamp()*2) * 0.05;
		}
		fieldTileLayer.render();
	}

	public function getFieldWidth(): Int {
		return field.width;
	}

	public function getFieldHeight(): Int {
		return field.height;
	}

	public function testCase(): Void {
		set(createTile(0), 0, 0);
		set(createTile(0), 1, 0);
		set(createTile(2), 2, 0);
		set(createTile(0), 3, 0);
		set(createTile(0), 4, 0);

		set(createTile(2), 0, 1);
		set(createTile(2), 1, 1);
		set(createTile(2), 2, 1);
		set(createTile(2), 3, 1);
		set(createTile(2), 4, 1);

		set(createTile(5), 0, 2);
		set(createTile(5), 1, 2);
		set(createTile(2), 2, 2);
		set(createTile(5), 3, 2);
		set(createTile(5), 4, 2);

		set(createTile(0), 0, 3);
		set(createTile(1), 1, 3);
		set(createTile(2), 2, 3);
		set(createTile(3), 3, 3);
		set(createTile(4), 4, 3);

		set(createTile(0), 0, 4);
		set(createTile(1), 1, 4);
		set(createTile(2), 2, 4);
		set(createTile(3), 3, 4);
		set(createTile(4), 4, 4);

		++busy;
		Actuate.timer(0.02).onComplete(function():Void {
			nextCheck = true;
			--busy;
		});
	}

	public function cheat(): Void {
		set(createTile(0), 0, 0);
		set(createTile(0), 1, 0);
		set(createTile(0), 2, 0);
		set(createTile(0), 3, 0);
		set(createTile(0), 4, 0);

		set(createTile(1), 0, 1);
		set(createTile(1), 1, 1);
		set(createTile(1), 2, 1);
		set(createTile(1), 3, 1);
		set(createTile(1), 4, 1);

		set(createTile(2), 0, 2);
		set(createTile(2), 1, 2);
		set(createTile(2), 2, 2);
		set(createTile(2), 3, 2);
		set(createTile(2), 4, 2);

		set(createTile(0), 0, 3);
		set(createTile(1), 1, 3);
		set(createTile(2), 2, 3);
		set(createTile(3), 3, 3);
		set(createTile(4), 4, 3);

		set(createTile(0), 0, 4);
		set(createTile(1), 1, 4);
		set(createTile(2), 2, 4);
		set(createTile(3), 3, 4);
		set(createTile(4), 4, 4);

		++busy;
		Actuate.timer(0.02).onComplete(function():Void {
			nextCheck = true;
			--busy;
		});
	}

	public function reposition(tileSize_: Float): Void {
		tileSize = tileSize_;
		tileHalfSize = tileSize_ / 2;

		if (fieldBackground.bitmapData.width != Std.int(field.width * tileSize)){
			fieldBackground.bitmapData.dispose();
			fieldBackground.bitmapData = Util.getResizedBitmapData("field", Std.int(field.width * tileSize), Std.int(field.height * tileSize));
		}

		fieldTileLayer.tilesheet = Tile.sheet;
		fieldContainer.init(fieldTileLayer);
		for (y in 0...field.height) {
			for (x in 0...field.width) {
				var tile: Tile = field.get(x, y);
				if (tile != null) {
					#if flash
						fieldContainer.removeChild(tile);
						tile = createTile(tile.tileColor);
					#end
					set(tile, x, y);
				}
			}
		}

		#if flash
			var idx = fieldTileLayer.getChildIndex(blindSprite);
			fieldTileLayer.removeChild(blindSprite);
			var old = blindSprite;
			blindSprite = new TileSprite("blind");
			blindSprite.visible = old.visible;
			blindSprite.scaleX = old.scaleX;
			blindSprite.scaleY = old.scaleY;
			blindSprite.alpha = old.alpha;
			blindSprite.rotation = old.rotation;
			fieldTileLayer.addChildAt(blindSprite, idx);
		#end
		blindSprite.x = field.width * tileSize * 0.5;
		blindSprite.y = field.height * tileSize * 0.5;

		fieldTileLayer.render();
	}

	private function startCritical(): Void {
		if (!nowCritical){
			nowCritical = true;
			dispatchEvent(new Event(START_CRITICAL));
		}
	}

	private function endCritical(): Void {
		if (nowCritical){
			nowCritical = false;
			dispatchEvent(new Event(END_CRITICAL));
		}
	}

	private function mouseDown(e: MouseEvent): Void {
		//trace('mouseDown');
		#if js
			Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
		#else
			e.target.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
		#end

		oldMouseTilePos.x = Std.int(Math.max(0, Math.min(field.width-1, e.localX / tileSize)));
		oldMouseTilePos.y = Std.int(Math.max(0, Math.min(field.height-1, e.localY / tileSize)));
		inputActive = true;
	}
	
	private function mouseUp(e: MouseEvent): Void {
		//trace('mouseUp');
		#if js
			Lib.current.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		#else
			e.target.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUp);
		#end

		inputActive = false;
	}

	private function mouseMove(e: MouseEvent): Void {			
		//trace('mouseMove');
		if (!inputActive) {
			return;
		}

		var nowTileX: Int = Std.int(Math.max(0, Math.min(field.width-1, e.localX / tileSize)));
		var nowTileY: Int  = Std.int(Math.max(0, Math.min(field.height-1, e.localY / tileSize)));
		while ((oldMouseTilePos.x != nowTileX || oldMouseTilePos.y != nowTileY) && !nowCritical && inputEnable) {
			var v: Float = Math.atan2(oldMouseTilePos.y - nowTileY, oldMouseTilePos.x - nowTileX) / Math.PI * 180;
			if ( -45 < v && v <= 45) {
				switch(chaosMove) {
					case Normal:
						slide(SlideTo.Left(nowTileY));
					case Reverse:
						slide(SlideTo.Right(nowTileY));
					case Cross:
						slide(SlideTo.Left(nowTileY));
						slide(SlideTo.Down(nowTileX));
				}
				oldMouseTilePos.x -= 1;
			} else if (45 < v && v <= 135) {
				switch(chaosMove) {
					case Normal:
						slide(SlideTo.Up(nowTileX));
					case Reverse:
						slide(SlideTo.Down(nowTileX));
					case Cross:
						slide(SlideTo.Up(nowTileX));
						slide(SlideTo.Right(nowTileY));
				}
				oldMouseTilePos.y -= 1;
			} else if (135 < v || v < -135) {
				switch(chaosMove) {
					case Normal:
						slide(SlideTo.Right(nowTileY));
					case Reverse:
						slide(SlideTo.Left(nowTileY));
					case Cross:
						slide(SlideTo.Right(nowTileY));
						slide(SlideTo.Up(nowTileX));
				}
				oldMouseTilePos.x += 1;
			} else {
				switch(chaosMove) {
					case Normal:
						slide(SlideTo.Down(nowTileX));
					case Reverse:
						slide(SlideTo.Up(nowTileX));
					case Cross:
						slide(SlideTo.Down(nowTileX));
						slide(SlideTo.Left(nowTileY));
				}
				oldMouseTilePos.y += 1;
			}
			
			if (findErasable().length != 0) {
				//消せる時は通り過ぎないようにフラグを立ててスライドを禁止させる
				startCritical();
				break;
			}
		}
	}
	
	/**
	 * 盤面上のタイルをすべて消す（アニメーション付き）
	 */
	public function drop(): Void {
		var anim: Float = 0.4;
		var delay: Float = 0.03;
		var ease: IEasing = Back.easeIn;
		var allNull: Bool = true;
		
		//スライドアニメーション中などに割り込むと詰めなおしされてしまうので
		//強制的にキャンセルする
		nextCheck = false;

		for (y in 0...field.height) {
			for (x in 0...field.width) {
				var tile: Tile = field.get(x, y);
				set(null, x, y);
				if (tile == null) {
					continue;
				}
				allNull = false;
				effectContainer.addChild(tile);
				animateTile(tile, [1, y], [0, y + 1], function(tile: Tile, v: Float, y: Float):Void {
					tile.scale = v;
					tile.alpha = v;
					tile.fieldY = y;
				}, function(tile: Tile):Void {
					effectContainer.removeChild(tile);
				}, anim, ease).delay((y * field.height + x) * delay);
			}
		}
		if (!allNull) {
			dispatchEvent(new Event(Field.DROP));
		}
	}

	/**
	 * 盤面上にあるタイルの色をすり替える
	 * @param	from
	 * @param	to
	 */
	public function swapColor(from: Int, to: Int): Void {
		startCritical();

		for (y in 0...field.height) {
			for (x in 0...field.width) {
				var tile: Tile = field.get(x, y);
				if (tile != null && tile.tileColor == from) {
					set(createTile(to), x, y);
				}
			}
		}
		
		//TODO: すり替えた時のエフェクト

		++busy;
		Actuate.timer(0.02).onComplete(function():Void {
			nextCheck = true;
			--busy;
		});
	}

	public function buildCombo(): Void {
		var combo = Combo.get(rand, field.colors, field.width, field.height);
		for (y in 0...field.height) {
			for (x in 0...field.width) {
				//前のタイルを消す
				var tile: Tile = field.get(x, y);
				if (tile != null) {
					eraseTile(x, y, (x + y * field.width) * 0.01);
				}
				
				//新しいタイルを設置する
				var fromX: Float;
				var fromY: Float;
				if (rand.next() > 0.5) {
					fromX = if (rand.next() > 0.5) -1 else field.width;
					fromY = rand.next() * field.height;
				} else {
					fromX = rand.next() * field.width;
					fromY = if (rand.next() > 0.5) -1 else field.height;
				}
				var newTile = combo.get(x, y);
				set(newTile, x, y);
				animateTile(newTile, [fromX, fromY, 0], [x, y, 1],
					function(tile: Tile, x: Float, y: Float, a: Float): Void {
						tile.fieldX = x;
						tile.fieldY = y;
						tile.alpha = a;
					},
					null,
					0.5,
					Cubic.easeOut
				).delay((field.width * field.height + x + y * field.width) * 0.01);
			}
		}
		nextCheck = true;
		startCritical();
	}

	private function createTile(color: Int): Tile {
		if (color == field.colors) {
			return new SpecialTile(color);
		} else {
			return new Tile(color);
		}
	}
	
	/**
	 * タイルを並べる
	 * @param	colors
	 */
	public function init(seed: Int): Void {
		rand.setSeed(seed);

		for (y in 0...field.height) {
			for (x in 0...field.width) {
				set(null, x, y);
			}
		}

		++busy;
		Actuate.timer(0.02).onComplete(function():Void {
			nextCheck = true;
			--busy;
		});
	}

	private function setBlind(v: Bool): Bool {
		if (blind != v) {
			if (v) {
				Actuate.update(function(a: Float, s: Float): Void {
					blindSprite.visible = true;
					blindSprite.alpha = a;
					blindSprite.scaleX = s;
					blindSprite.scaleY = s;
				}, 0.5, [0, 1.25], [1, 1], false).ease(Cubic.easeIn).onComplete(function(): Void {
				});
			} else {
				Actuate.update(function(a: Float, s: Float): Void {
					blindSprite.visible = true;
					blindSprite.alpha = a;
					blindSprite.scaleX = s;
					blindSprite.scaleY = s;
				}, 0.5, [1, 1], [0, 1.25], false).ease(Cubic.easeOut).onComplete(function(): Void {
					blindSprite.visible = false;
				});
			}
		}
		blind = v;
		return v;
	}

	/**
	 * アニメーション状態管理
	 * アニメーションが終わった時自動的に READY などを呼ぶ
	 * @param	v
	 * @return
	 */
	private function setBusy(v: Int): Int {
		var oldbusy = busy;

		busy = v;

		if (oldbusy == 1 && v == 0) {
			dispatchEvent(new Event(Field.READY));
		}

		return v;
	}

	/**
	 * タイルのインスタンスを設置
	 * null をセットすると消える
	 * @param	tile
	 * @param	x
	 * @param	y
	 */
	private function set(tile: Tile, x: Int, y: Int): Void {
		var t: Tile = field.get(x, y);
		if (t != null){
			fieldContainer.removeChild(t);
		}
		field.set(x, y, tile);
		if (tile != null) {
			tile.fieldX = x;
			tile.fieldY = y;
			fieldContainer.addChild(tile);
		}
	}

	/**
	 * タイルを入れ替える
	 * @param	x1
	 * @param	y1
	 * @param	x2
	 * @param	y2
	 */
	private function exchange(x1: Int, y1: Int, x2: Int, y2: Int): Void {
		var t1: Tile = field.get(x1, y1);
		var t2: Tile = field.get(x2, y2);
		field.set(x1, y1, t2);
		field.set(x2, y2, t1);
		if (t1 != null) {
			t1.fieldX = x2;
			t1.fieldY = y2;
		}
		if (t2 != null) {
			t2.fieldX = x1;
			t2.fieldY = y1;
		}
	}

	private function animateTile(tile: Tile, from: Array<Dynamic>, to: Array<Dynamic>, proc: Dynamic, complete: Tile -> Void, anim: Float = 0.05, ease: IEasing = null): GenericActuator {
		from.unshift(tile);
		to.unshift(tile);

		++busy;
		Reflect.callMethod(this, proc, from);
		var a = cast(Actuate.update(proc, anim, from, to, false).onComplete (function(): Void {
			if (complete != null) {
				complete(tile);
			}
			--busy;
		}), GenericActuator);
		if (ease != null) {
			a.ease(ease);
		}
		
		return a;
	}

	/**
	 * タイルのスライド
	 * @param	v
	 */
	private function slide(v: SlideTo): Void {
		var anim: Float = 0.05;
		var ease = Linear.easeNone;

		switch(v) {
			case Up(x):
				for (y in 1...field.height) {
					exchange(x, y - 1, x, y);
				}

				//アニメーション
				var tile: Tile;
				for (y in 0...field.height) {
					tile = field.get(x, y);
					animateTile(tile, [tile.fieldY + 1], [tile.fieldY], function(tile: Tile, v: Float):Void {
						tile.fieldY = v;
					}, null, anim, ease);
				}
				tile = createTile(field.get(x, field.height - 1).tileColor);
				tile.fieldX = x;
				effectContainer.addChild(tile);
				animateTile(tile, [0], [-1], function(tile: Tile, v: Float):Void {
					tile.fieldY = v;
				}, function(tile: Tile): Void {
					effectContainer.removeChild(tile);
				}, anim, ease);
			case Down(x):
				for (y in 1...field.height) {
					exchange(x, field.height - y, x, field.height - y - 1);
				}

				//アニメーション
				var tile: Tile;
				for (y in 0...field.height) {
					tile = field.get(x, y);
					animateTile(tile, [tile.fieldY - 1], [tile.fieldY], function(tile: Tile, v: Float):Void {
						tile.fieldY = v;
					}, null, anim, ease);
				}
				tile = createTile(field.get(x, 0).tileColor);
				tile.fieldX = x;
				effectContainer.addChild(tile);
				animateTile(tile, [field.height - 1], [field.height], function(tile: Tile, v: Float):Void {
					tile.fieldY = v;
				}, function(tile: Tile): Void {
					effectContainer.removeChild(tile);
				}, anim, ease);
			case Left(y):
				for (x in 1...field.width) {
					exchange(x - 1, y, x, y);
				}

				//アニメーション
				var tile: Tile;
				for (x in 0...field.width) {
					tile = field.get(x, y);
					animateTile(tile, [tile.fieldX + 1], [tile.fieldX], function(tile: Tile, v: Float):Void {
						tile.fieldX = v;
					}, null, anim, ease);
				}
				tile = createTile(field.get(field.width - 1, y).tileColor);
				tile.fieldY = y;
				effectContainer.addChild(tile);
				animateTile(tile, [0], [-1], function(tile: Tile, v: Float):Void {
					tile.fieldX = v;
				}, function(tile: Tile): Void {
					effectContainer.removeChild(tile);
				}, anim, ease);
			case Right(y):
				for (x in 1...field.width) {
					exchange(field.width - x, y, field.width - x - 1, y);
				}

				//アニメーション
				var tile: Tile;
				for (x in 0...field.width) {
					tile = field.get(x, y);
					animateTile(tile, [tile.fieldX - 1], [tile.fieldX], function(tile: Tile, v: Float):Void {
						tile.fieldX = v;
					}, null, anim, ease);
				}
				tile = createTile(field.get(0, y).tileColor);
				tile.fieldY = y;
				effectContainer.addChild(tile);
				animateTile(tile, [field.width - 1], [field.width], function(tile: Tile, v: Float):Void {
					tile.fieldX = v;
				}, function(tile: Tile): Void {
					effectContainer.removeChild(tile);
				}, anim, ease);
		}
		dispatchEvent(new SlideEvent(v));
		nextCheck = true;
	}

	/**
	 * 消せるラインがあれば探して消す
	 * 空いた空間を埋める処理はアニメーション終了後に pad で行う
	 */
	private function ready(e: Dynamic): Void {
		if (!nextCheck) {
			return;
		}
		nextCheck = false;

		startCritical();

		var erased: Int = erase();
		++busy;
		Actuate.timer(0.02).onComplete(function():Void {
			if (erased != 0) {
				//消せた場合は寄せる
				addEventListener(Field.READY, pad);
			} else {
				//消せない場合は余白を埋める
				addEventListener(Field.READY, fill);
			}
			--busy;
		});
	}

	/**
	 * 消せる列があれば消す
	 * @return 消した列数
	 */
	private function erase(): Int {
		var delay: Float = 0.05;
		var founds: Array<Line> = findErasable();
		var d: Float = 0;
		var tile: Tile;

		//特定の状況下で発生する縦と横が交差する消去パターンでないか調べる
		var hasH: Bool = false;
		var hasV: Bool = false;
		for (i in 0...founds.length) {
			switch(founds[i]) {
				case Horizontal(y):
					hasH = true;
				case Vertical(x):
					hasV = true;
			}
		}
		if (hasH && hasV) {
			//交差パターンの場合は全体で1回分の消去としてカウント
			++chain;
			Actuate.timer(d).onComplete(function(chain: Int): Void {
				dispatchEvent(new EraseEvent(Field.ERASING, chain));
			}, [chain]);
			for (i in 0...founds.length) {
				d = 0;
				switch(founds[i]) {
					case Horizontal(y):
						for (x in 0...field.width) {
							tile = field.get(x, y);
							if (tile != null) {
								eraseTile(x, y, d);
							}
							d += delay;
						}
					case Vertical(x):
						for (y in 0...field.height) {
							tile = field.get(x, y);
							if (tile != null) {
								eraseTile(x, y, d);
							}
							d += delay;
						}
				}
			}
			Actuate.timer(d).onComplete(function(chain: Int): Void {
				dispatchEvent(new EraseEvent(Field.ERASED, chain));
			}, [chain]);
			return 1;
		} else {
			var ct: Int = 0;
			for (i in 0...founds.length) {
				++chain;
				Actuate.timer(d).onComplete(function(chain: Int): Void {
					dispatchEvent(new EraseEvent(Field.ERASING, chain));
				}, [chain]);
				ct = 0;
				switch(founds[i]) {
					case Horizontal(y):
						for (x in 0...field.width) {
							tile = field.get(x, y);
							if (tile != null) {
								eraseTile(x, y, d += delay);
								++ct;
							}
						}
					case Vertical(x):
						for (y in 0...field.height) {
							tile = field.get(x, y);
							if (tile != null) {
								eraseTile(x, y, d += delay);
								++ct;
							}
						}
				}
				d += delay * (5 - ct + 4);
				Actuate.timer(d).onComplete(function(chain: Int): Void {
					dispatchEvent(new EraseEvent(Field.ERASED, chain));
				}, [chain]);
			}
			return founds.length;
		}
	}

	/**
	 * タイルを1枚消す
	 * @param	x
	 * @param	y
	 * @return
	 */
	private function eraseTile(x: Int, y: Int, delay: Float): Void {
		var anim: Float = 0.2;
		var ease: IEasing = Back.easeIn;

		//単なるアニメーションデータとして持ち直す
		var tile: Tile = field.get(x, y);
		set(null, x, y);
		effectContainer.addChild(tile);
		animateTile(tile, [1], [0], function(tile: Tile, v: Float):Void {
			tile.scale = v;
		}, function(tile: Tile):Void {
			effectContainer.removeChild(tile);
		}, anim, ease).delay(delay);

		//オーバーレイエフェクト
		Actuate.timer(delay).onComplete(function(): Void {
			var oltile: Tile = createTile(tile.tileColor);
			effectContainer.addChild(oltile);
			oltile.fieldX = x;
			oltile.fieldY = y;
			animateTile(oltile, [1, 1], [1.5, 0], function(oltile: Tile, s: Float, v: Float):Void {
				oltile.scale = s;
				oltile.alpha = v;
			}, function(oltile: Tile):Void {
				effectContainer.removeChild(oltile);
			}, anim*2, Sine.easeOut);
		});
	}

	/**
	 * 消せるタイルを探して配列で返す
	 * @return
	 */
	private function findErasable(): Array<Line> {
		//消す対象のパネル
		var founds: Array<Line> = [];

		//横方向に消せるものがあるか探す
		for (y in 0...field.height) {
			var lineTile: Tile = null;
			var same: Int = 0;
			for (x in 0...field.width) {
				var tile: Tile = field.get(x, y);
				if (tile != null) {
					if (lineTile == null) {
						lineTile = tile;
					} else if (Tile.isSameColor(lineTile, tile)) {
						if (Type.getClass(lineTile) == SpecialTile) {
							lineTile = tile;
						}
					} else {
						same = 0;
						break;
					}
					++same;
				}
			}
			if (same > 1 && lineTile != null) {
				founds.push(Line.Horizontal(y));
			}
		}
		
		//縦方向に消せるものがあるか探す
		for (x in 0...field.width) {
			var lineTile: Tile = null;
			var same: Int = 0;
			for (y in 0...field.height) {
				var tile: Tile = field.get(x, y);
				if (tile != null) {
					if (lineTile == null) {
						lineTile = tile;
					} else if (Tile.isSameColor(lineTile, tile)) {
						if (Type.getClass(lineTile) == SpecialTile) {
							lineTile = tile;
						}
					} else {
						same = 0;
						break;
					}
					++same;
				}
			}
			if (same > 1 && lineTile != null) {
				founds.push(Line.Vertical(x));
			}
		}

		return founds;
	}

	/**
	 * 空いた空間を寄せるアニメーション
	 * @param	e
	 */
	private function pad(e: Dynamic): Void {
		removeEventListener(Field.READY, pad);

		var anim: Float = 0.15;
		var delay: Float = 0.02;
		var ease: IEasing = Back.easeIn;

		//移動前と移動後の対応表を作る
		var newPos: Array<IntPoint> = [];
		for (y in 0...field.height) {
			for (x in 0...field.width) {
				newPos.push(new IntPoint(x, y));
			}
		}
		
		//Y軸方向に寄せる
		var pad: Int = 0;
		for (y in 0...field.height) {
			var emptyLine: Bool = true;
			for (x in 0...field.width) {
				if (!emptyLine || field.get(x, y) != null) {
					emptyLine = false;
					exchange(x, y, x, y - pad);
					newPos[(y - pad) * field.height + x].y = y;
				}
			}
			if (emptyLine) {
				++pad;
			}
		}

		//X軸方向に寄せる
		var pad: Int = 0;
		for (x in 0...field.width) {
			var emptyLine: Bool = true;
			for (y in 0...field.height) {
				if (!emptyLine || field.get(x, y) != null) {
					emptyLine = false;
					exchange(x, y, x - pad, y);
					newPos[y * field.height + (x - pad)].x = x;
				}
			}
			if (emptyLine) {
				++pad;
			}
		}

		//対応表に基づいて再配置のアニメーション
		var i: Int = 0;
		for (y in 0...field.height) {
			for (x in 0...field.width) {
				var p: IntPoint = newPos[y * field.height + x];
				if (p.x != x || p.y != y) {
					//変更があったらアニメーション
					var tile: Tile = field.get(x, y);
					if (tile != null) {
						animateTile(tile, [p.x, p.y], [x, y], function(tile: Tile, x: Float, y: Float):Void {
							tile.fieldX = x;
							tile.fieldY = y;
						}, null, anim, ease).delay(i * delay);
						++i;
					}
				}
			}
		}

		++busy;
		Actuate.timer(0.02).onComplete(function():Void {
			nextCheck = true;
			--busy;
		});
	}

	/**
	 * 空いてるマスを埋める処理
	 * scale を使うと何故か HTML5 でタイルが消えてしまうので別なエフェクトで代用……
	 */
	private function fill(e: Dynamic): Void {
		removeEventListener(Field.READY, fill);

		var anim: Float = 0.1;
		var delay: Float = 0.025;
		var ease: IEasing = Linear.easeNone;

		//空いているマスを調べる
		var tiles: Array<IntPoint> = [];
		for (y in 0...field.height) {
			for (x in 0...field.width) {
				var tile: Tile = field.get(x, y);
				if (tile == null) {
					tiles.push(new IntPoint(x, y));
				}
			}
		}
		if (tiles.length == 0)
		{
			chain = 0;
			endCritical();
			return;
		}

		var addTiles: Array<Tile> = getAdditionalTiles(tiles.length, chain != 0);
		for (i in 0...tiles.length) {
			var pos: IntPoint = tiles[i];
			var tile: Tile = addTiles[i];
			set(tile, pos.x, pos.y);
			animateTile(tile, [tile.fieldY + 0.5, 0], [tile.fieldY, 1], function(tile: Tile, y: Float, a: Float):Void {
				tile.fieldY = y;
				tile.alpha = a;
			}, null, anim, ease).delay(i * delay);
		}

		chain = 0;

		++busy;
		Actuate.timer(0.02).onComplete(function():Void {
			nextCheck = true;
			--busy;
		});
	}

	/**
	 * 指定された枚数分追加すべきタイルを生成して返す
	 * @param	num
	 * @return
	 */
	private function getAdditionalTiles(num: Int, includeSpecialPiece: Bool): Array<Tile> {
		//一番枚数が少ないタイルは何枚ある？
		var tileCount: Array<Int> = getNumColors();
		var tiles: Array<Int> = [];
		var min: Int = field.width * field.height;
		for (i in 0...tileCount.length) {
			if (min > tileCount[i]) {
				min = tileCount[i];
				tiles = [i];
			} else if (tileCount[i] == min) {
				tiles.push(i);
			}
		}
		
		//その中から適当に1枚選んで、そこを始点に追加するタイルを選ぶ
		Util.shuffleArray(rand, tiles);

		var ret: Array<Tile> = [];
		
		if (includeSpecialPiece) {
			--num;
			ret.push(createTile(field.colors));
		}
		
		var c: Int = tiles[0];
		for (i in 0...num) {
			ret.push(createTile(c));
			c = (c + 1) % field.colors;
		}
		
		Util.shuffleArray(rand, ret);
		return ret;
	}

	/**
	 * 盤面上にどの色が何枚あるかカウントして返す
	 * @return
	 */
	private function getNumColors(): Array<Int> {
		var ret: Array<Int> = [];
		
		for (i in 0...field.colors) {
			ret.push(0);
		}
		
		for (y in 0...field.height) {
			for (x in 0...field.width) {
				var tile: Tile = field.get(x, y);
				if (tile != null && Type.getClass(tile) == Tile) {
					++ret[tile.tileColor];
				}
			}
		}
		
		return ret;
	}
}