package ;
import nme.events.Event;

/**
 * ...
 * @author oov
 */

class EraseEvent extends Event
{
	public var chain: Int;
	public function new(type: String, chain_: Int) {
		super(type);
		chain = chain_;
	}
}